const express = require("express");
const morgan = require("morgan");
const cors = require("cors");
const errorHandler = require("./src/middlewares/error");
const connectDB = require("./src/mongo/index");
const CronJob = require('node-cron');
const app = express();
app.use(cors());

const userRouter = require("./src/routes/application/user");
const ownerRouter = require("./src/routes/application/enquiry");
const KYCRouter = require("./src/routes/application/kycDetail");
const adminUserRouter = require("./src/routes/admin/useradmin");
const facilitiesAndServicesRouter = require("./src/routes/admin/facilitiesAndServices");
const adminOwnerRouter = require("./src/routes/admin/owner");
const stateRouter = require("./src/routes/admin/state");
const cityRouter = require("./src/routes/admin/city");
const pgroutes = require("./src/routes/application/pgRoutes");
const ticketroutes = require("./src/routes/application/ticketRequest")
const advancePaymentRoutes = require("./src/routes/payment/paymentAdvance")
const notificationRouter = require("./src/routes/application/notification")



//env
const dotenv = require("dotenv");
const { updateStateId } = require("./src/helpers/cronjobFunction");
dotenv.config({ path: "./src/config/config.env" });

// body parser
app.use(express.json());


// connect to database
connectDB();

// concat all routes
let allRoutes = [].concat(
    userRouter,
    ownerRouter,
    adminUserRouter,
    adminOwnerRouter,
    KYCRouter,
    stateRouter,
    facilitiesAndServicesRouter,
    cityRouter,
    pgroutes, ticketroutes, advancePaymentRoutes,
    notificationRouter
)

// body parser
app.use(express.json());

// dev logging middleware

if (process.env.NODE_ENV === "development") {
    app.use(morgan("dev"));
}

// mount router
app.use("/api/v1", allRoutes);

app.use(errorHandler);
// error handler
app.use(function (err, req, res, next) {

    return res.json({
        success: false,
        message: "Error Occured" + err
    })
})

const PORT = process.env.PORT || 3000;

const server = app.listen(
    PORT,
    console.log(`server runing in ${process.env.NODE_ENV} mode on port ${PORT}`)
);

// handle promise rejection

process.on("unhandledRejection", (err, promise) => {
    console.log(`Error : ${err.message}`);
    // close server and exit process
    server.close(() => process.exit(1));
});

//CronJob
CronJob.schedule('0 44 17 * * *', async () => {
    console.info(`running cron job a task ${new Date()}`);

    // await updateStateId();

})
