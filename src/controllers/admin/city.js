const city = require("../../models/admin/city");
const pgDetails = require("../../models/admin/pgSchema")

module.exports.createCityList = async (req, res, next) => {

    var cityObj = [
        {
            "_id": "1",
            "cityName": "Mumbai",
            "stateName": "Maharashtra"
        },
        {
            "_id": "2",
            "cityName": "Delhi",
            "stateName": "Delhi"
        },
        {
            "_id": "3",
            "cityName": "Bengaluru",
            "stateName": "Karnataka"
        },
        {
            "_id": "4",
            "cityName": "Ahmedabad",
            "stateName": "Gujarat"
        },
        {
            "_id": "5",
            "cityName": "Hyderabad",
            "stateName": "Telangana"
        },
        {
            "_id": "6",
            "cityName": "Chennai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "7",
            "cityName": "Kolkata",
            "stateName": "West Bengal"
        },
        {
            "_id": "8",
            "cityName": "Pune",
            "stateName": "Maharashtra"
        },
        {
            "_id": "9",
            "cityName": "Jaipur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "10",
            "cityName": "Surat",
            "stateName": "Gujarat"
        },
        {
            "_id": "11",
            "cityName": "Lucknow",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "12",
            "cityName": "Kanpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "13",
            "cityName": "Nagpur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "14",
            "cityName": "Patna",
            "stateName": "Bihar"
        },
        {
            "_id": "15",
            "cityName": "Indore",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "16",
            "cityName": "Thane",
            "stateName": "Maharashtra"
        },
        {
            "_id": "17",
            "cityName": "Bhopal",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "18",
            "cityName": "Visakhapatnam",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "19",
            "cityName": "Vadodara",
            "stateName": "Gujarat"
        },
        {
            "_id": "20",
            "cityName": "Firozabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "21",
            "cityName": "Ludhiana",
            "stateName": "Punjab"
        },
        {
            "_id": "22",
            "cityName": "Rajkot",
            "stateName": "Gujarat"
        },
        {
            "_id": "23",
            "cityName": "Agra",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "24",
            "cityName": "Siliguri",
            "stateName": "West Bengal"
        },
        {
            "_id": "25",
            "cityName": "Nashik",
            "stateName": "Maharashtra"
        },
        {
            "_id": "26",
            "cityName": "Far_idabad",
            "stateName": "Haryana"
        },
        {
            "_id": "27",
            "cityName": "Patiala",
            "stateName": "Punjab"
        },
        {
            "_id": "28",
            "cityName": "Meerut",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "29",
            "cityName": "Kalyan-Dombivali",
            "stateName": "Maharashtra"
        },
        {
            "_id": "30",
            "cityName": "Vasai-Virar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "31",
            "cityName": "Varanasi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "32",
            "cityName": "Srinagar",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "33",
            "cityName": "Dhanbad",
            "stateName": "Jharkhand"
        },
        {
            "_id": "34",
            "cityName": "Jodhpur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "35",
            "cityName": "Amritsar",
            "stateName": "Punjab"
        },
        {
            "_id": "36",
            "cityName": "Raipur",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "37",
            "cityName": "Allahabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "38",
            "cityName": "Coimbatore",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "39",
            "cityName": "Jabalpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "40",
            "cityName": "Gwalior",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "41",
            "cityName": "Vijayawada",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "42",
            "cityName": "Madurai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "43",
            "cityName": "Guwahati",
            "stateName": "Assam"
        },
        {
            "_id": "44",
            "cityName": "Chandigarh",
            "stateName": "Chandigarh"
        },
        {
            "_id": "45",
            "cityName": "Hubli-Dharwad",
            "stateName": "Karnataka"
        },
        {
            "_id": "46",
            "cityName": "Amroha",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "47",
            "cityName": "Moradabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "48",
            "cityName": "Gurgaon",
            "stateName": "Haryana"
        },
        {
            "_id": "49",
            "cityName": "Aligarh",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "50",
            "cityName": "Solapur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "51",
            "cityName": "Ranchi",
            "stateName": "Jharkhand"
        },
        {
            "_id": "52",
            "cityName": "Jalandhar",
            "stateName": "Punjab"
        },
        {
            "_id": "53",
            "cityName": "Tiruchirappalli",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "54",
            "cityName": "Bhubaneswar",
            "stateName": "Odisha"
        },
        {
            "_id": "55",
            "cityName": "Salem",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "56",
            "cityName": "Warangal",
            "stateName": "Telangana"
        },
        {
            "_id": "57",
            "cityName": "Mira-Bhayandar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "58",
            "cityName": "Thiruvananthapuram",
            "stateName": "Kerala"
        },
        {
            "_id": "59",
            "cityName": "Bhiwandi",
            "stateName": "Maharashtra"
        },
        {
            "_id": "60",
            "cityName": "Saharanpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "61",
            "cityName": "Guntur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "62",
            "cityName": "Amravati",
            "stateName": "Maharashtra"
        },
        {
            "_id": "63",
            "cityName": "Bikaner",
            "stateName": "Rajasthan"
        },
        {
            "_id": "64",
            "cityName": "No_ida",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "65",
            "cityName": "Jamshedpur",
            "stateName": "Jharkhand"
        },
        {
            "_id": "66",
            "cityName": "Bhilai Nagar",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "67",
            "cityName": "Cuttack",
            "stateName": "Odisha"
        },
        {
            "_id": "68",
            "cityName": "Kochi",
            "stateName": "Kerala"
        },
        {
            "_id": "69",
            "cityName": "Udaipur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "70",
            "cityName": "Bhavnagar",
            "stateName": "Gujarat"
        },
        {
            "_id": "71",
            "cityName": "Dehradun",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "72",
            "cityName": "Asansol",
            "stateName": "West Bengal"
        },
        {
            "_id": "73",
            "cityName": "Nanded-Waghala",
            "stateName": "Maharashtra"
        },
        {
            "_id": "74",
            "cityName": "Ajmer",
            "stateName": "Rajasthan"
        },
        {
            "_id": "75",
            "cityName": "Jamnagar",
            "stateName": "Gujarat"
        },
        {
            "_id": "76",
            "cityName": "Ujjain",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "77",
            "cityName": "Sangli",
            "stateName": "Maharashtra"
        },
        {
            "_id": "78",
            "cityName": "Loni",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "79",
            "cityName": "Jhansi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "80",
            "cityName": "Pondicherry",
            "stateName": "Puducherry"
        },
        {
            "_id": "81",
            "cityName": "Nellore",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "82",
            "cityName": "Jammu",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "83",
            "cityName": "Belagavi",
            "stateName": "Karnataka"
        },
        {
            "_id": "84",
            "cityName": "Raurkela",
            "stateName": "Odisha"
        },
        {
            "_id": "85",
            "cityName": "Mangaluru",
            "stateName": "Karnataka"
        },
        {
            "_id": "86",
            "cityName": "Tirunelveli",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "87",
            "cityName": "Malegaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "88",
            "cityName": "Gaya",
            "stateName": "Bihar"
        },
        {
            "_id": "89",
            "cityName": "Tiruppur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "90",
            "cityName": "Davanagere",
            "stateName": "Karnataka"
        },
        {
            "_id": "91",
            "cityName": "Kozhikode",
            "stateName": "Kerala"
        },
        {
            "_id": "92",
            "cityName": "Akola",
            "stateName": "Maharashtra"
        },
        {
            "_id": "93",
            "cityName": "Kurnool",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "94",
            "cityName": "Bokaro Steel City",
            "stateName": "Jharkhand"
        },
        {
            "_id": "95",
            "cityName": "Rajahmundry",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "96",
            "cityName": "Ballari",
            "stateName": "Karnataka"
        },
        {
            "_id": "97",
            "cityName": "Agartala",
            "stateName": "Tripura"
        },
        {
            "_id": "98",
            "cityName": "Bhagalpur",
            "stateName": "Bihar"
        },
        {
            "_id": "99",
            "cityName": "Latur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "100",
            "cityName": "Dhule",
            "stateName": "Maharashtra"
        },
        {
            "_id": "101",
            "cityName": "Korba",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "102",
            "cityName": "Bhilwara",
            "stateName": "Rajasthan"
        },
        {
            "_id": "103",
            "cityName": "Brahmapur",
            "stateName": "Odisha"
        },
        {
            "_id": "104",
            "cityName": "Mysore",
            "stateName": "Karnataka"
        },
        {
            "_id": "105",
            "cityName": "Muzaffarpur",
            "stateName": "Bihar"
        },
        {
            "_id": "106",
            "cityName": "Ahmednagar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "107",
            "cityName": "Kollam",
            "stateName": "Kerala"
        },
        {
            "_id": "108",
            "cityName": "Raghunathganj",
            "stateName": "West Bengal"
        },
        {
            "_id": "109",
            "cityName": "Bilaspur",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "110",
            "cityName": "Shahjahanpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "111",
            "cityName": "Thrissur",
            "stateName": "Kerala"
        },
        {
            "_id": "112",
            "cityName": "Alwar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "113",
            "cityName": "Kakinada",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "114",
            "cityName": "Nizamabad",
            "stateName": "Telangana"
        },
        {
            "_id": "115",
            "cityName": "Sagar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "116",
            "cityName": "Tumkur",
            "stateName": "Karnataka"
        },
        {
            "_id": "117",
            "cityName": "Hisar",
            "stateName": "Haryana"
        },
        {
            "_id": "118",
            "cityName": "Rohtak",
            "stateName": "Haryana"
        },
        {
            "_id": "119",
            "cityName": "Panipat",
            "stateName": "Haryana"
        },
        {
            "_id": "120",
            "cityName": "Darbhanga",
            "stateName": "Bihar"
        },
        {
            "_id": "121",
            "cityName": "Kharagpur",
            "stateName": "West Bengal"
        },
        {
            "_id": "122",
            "cityName": "Aizawl",
            "stateName": "Mizoram"
        },
        {
            "_id": "123",
            "cityName": "Ichalkaranji",
            "stateName": "Maharashtra"
        },
        {
            "_id": "124",
            "cityName": "Tirupati",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "125",
            "cityName": "Karnal",
            "stateName": "Haryana"
        },
        {
            "_id": "126",
            "cityName": "Bathinda",
            "stateName": "Punjab"
        },
        {
            "_id": "127",
            "cityName": "Rampur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "128",
            "cityName": "Shivamogga",
            "stateName": "Karnataka"
        },
        {
            "_id": "129",
            "cityName": "Ratlam",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "130",
            "cityName": "Modinagar",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "131",
            "cityName": "Durg",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "132",
            "cityName": "Shillong",
            "stateName": "Meghalaya"
        },
        {
            "_id": "133",
            "cityName": "Imphal",
            "stateName": "Manipur"
        },
        {
            "_id": "134",
            "cityName": "Hapur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "135",
            "cityName": "Ranipet",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "136",
            "cityName": "Anantapur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "137",
            "cityName": "Arrah",
            "stateName": "Bihar"
        },
        {
            "_id": "138",
            "cityName": "Karimnagar",
            "stateName": "Telangana"
        },
        {
            "_id": "139",
            "cityName": "Parbhani",
            "stateName": "Maharashtra"
        },
        {
            "_id": "140",
            "cityName": "Etawah",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "141",
            "cityName": "Bharatpur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "142",
            "cityName": "Begusarai",
            "stateName": "Bihar"
        },
        {
            "_id": "143",
            "cityName": "New Delhi",
            "stateName": "Delhi"
        },
        {
            "_id": "144",
            "cityName": "Chhapra",
            "stateName": "Bihar"
        },
        {
            "_id": "145",
            "cityName": "Kadapa",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "146",
            "cityName": "Ramagundam",
            "stateName": "Telangana"
        },
        {
            "_id": "147",
            "cityName": "Pali",
            "stateName": "Rajasthan"
        },
        {
            "_id": "148",
            "cityName": "Satna",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "149",
            "cityName": "Vizianagaram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "150",
            "cityName": "Katihar",
            "stateName": "Bihar"
        },
        {
            "_id": "151",
            "cityName": "Hardwar",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "152",
            "cityName": "Sonipat",
            "stateName": "Haryana"
        },
        {
            "_id": "153",
            "cityName": "Nagercoil",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "154",
            "cityName": "Thanjavur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "155",
            "cityName": "Murwara (Katni)",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "156",
            "cityName": "Naihati",
            "stateName": "West Bengal"
        },
        {
            "_id": "157",
            "cityName": "Sambhal",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "158",
            "cityName": "Nadiad",
            "stateName": "Gujarat"
        },
        {
            "_id": "159",
            "cityName": "Yamunanagar",
            "stateName": "Haryana"
        },
        {
            "_id": "160",
            "cityName": "English Bazar",
            "stateName": "West Bengal"
        },
        {
            "_id": "161",
            "cityName": "Eluru",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "162",
            "cityName": "Munger",
            "stateName": "Bihar"
        },
        {
            "_id": "163",
            "cityName": "Panchkula",
            "stateName": "Haryana"
        },
        {
            "_id": "164",
            "cityName": "Raayachuru",
            "stateName": "Karnataka"
        },
        {
            "_id": "165",
            "cityName": "Panvel",
            "stateName": "Maharashtra"
        },
        {
            "_id": "166",
            "cityName": "Deoghar",
            "stateName": "Jharkhand"
        },
        {
            "_id": "167",
            "cityName": "Ongole",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "168",
            "cityName": "Nandyal",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "169",
            "cityName": "Morena",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "170",
            "cityName": "Bhiwani",
            "stateName": "Haryana"
        },
        {
            "_id": "171",
            "cityName": "Porbandar",
            "stateName": "Gujarat"
        },
        {
            "_id": "172",
            "cityName": "Palakkad",
            "stateName": "Kerala"
        },
        {
            "_id": "173",
            "cityName": "Anand",
            "stateName": "Gujarat"
        },
        {
            "_id": "174",
            "cityName": "Purnia",
            "stateName": "Bihar"
        },
        {
            "_id": "175",
            "cityName": "Baharampur",
            "stateName": "West Bengal"
        },
        {
            "_id": "176",
            "cityName": "Barmer",
            "stateName": "Rajasthan"
        },
        {
            "_id": "177",
            "cityName": "Morvi",
            "stateName": "Gujarat"
        },
        {
            "_id": "178",
            "cityName": "Orai",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "179",
            "cityName": "Bahraich",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "180",
            "cityName": "Sikar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "181",
            "cityName": "Vellore",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "182",
            "cityName": "Singrauli",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "183",
            "cityName": "Khammam",
            "stateName": "Telangana"
        },
        {
            "_id": "184",
            "cityName": "Mahesana",
            "stateName": "Gujarat"
        },
        {
            "_id": "185",
            "cityName": "Silchar",
            "stateName": "Assam"
        },
        {
            "_id": "186",
            "cityName": "Sambalpur",
            "stateName": "Odisha"
        },
        {
            "_id": "187",
            "cityName": "Rewa",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "188",
            "cityName": "Unnao",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "189",
            "cityName": "Hugli-Chinsurah",
            "stateName": "West Bengal"
        },
        {
            "_id": "190",
            "cityName": "Raiganj",
            "stateName": "West Bengal"
        },
        {
            "_id": "191",
            "cityName": "Phusro",
            "stateName": "Jharkhand"
        },
        {
            "_id": "192",
            "cityName": "Adityapur",
            "stateName": "Jharkhand"
        },
        {
            "_id": "193",
            "cityName": "Alappuzha",
            "stateName": "Kerala"
        },
        {
            "_id": "194",
            "cityName": "Bahadurgarh",
            "stateName": "Haryana"
        },
        {
            "_id": "195",
            "cityName": "Machilipatnam",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "196",
            "cityName": "Rae Bareli",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "197",
            "cityName": "Jalpaiguri",
            "stateName": "West Bengal"
        },
        {
            "_id": "198",
            "cityName": "Bharuch",
            "stateName": "Gujarat"
        },
        {
            "_id": "199",
            "cityName": "Pathankot",
            "stateName": "Punjab"
        },
        {
            "_id": "200",
            "cityName": "Hoshiarpur",
            "stateName": "Punjab"
        },
        {
            "_id": "201",
            "cityName": "Baramula",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "202",
            "cityName": "Adoni",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "203",
            "cityName": "Jind",
            "stateName": "Haryana"
        },
        {
            "_id": "204",
            "cityName": "Tonk",
            "stateName": "Rajasthan"
        },
        {
            "_id": "205",
            "cityName": "Tenali",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "206",
            "cityName": "Kancheepuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "207",
            "cityName": "Vapi",
            "stateName": "Gujarat"
        },
        {
            "_id": "208",
            "cityName": "Sirsa",
            "stateName": "Haryana"
        },
        {
            "_id": "209",
            "cityName": "Navsari",
            "stateName": "Gujarat"
        },
        {
            "_id": "210",
            "cityName": "Mahbubnagar",
            "stateName": "Telangana"
        },
        {
            "_id": "211",
            "cityName": "Puri",
            "stateName": "Odisha"
        },
        {
            "_id": "212",
            "cityName": "Robertson Pet",
            "stateName": "Karnataka"
        },
        {
            "_id": "213",
            "cityName": "Erode",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "214",
            "cityName": "Batala",
            "stateName": "Punjab"
        },
        {
            "_id": "215",
            "cityName": "Haldwani-cum-Kathgodam",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "216",
            "cityName": "V_idisha",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "217",
            "cityName": "Saharsa",
            "stateName": "Bihar"
        },
        {
            "_id": "218",
            "cityName": "Thanesar",
            "stateName": "Haryana"
        },
        {
            "_id": "219",
            "cityName": "Chittoor",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "220",
            "cityName": "Veraval",
            "stateName": "Gujarat"
        },
        {
            "_id": "221",
            "cityName": "Lakhimpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "222",
            "cityName": "Sitapur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "223",
            "cityName": "Hindupur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "224",
            "cityName": "Santipur",
            "stateName": "West Bengal"
        },
        {
            "_id": "225",
            "cityName": "Balurghat",
            "stateName": "West Bengal"
        },
        {
            "_id": "226",
            "cityName": "Ganjbasoda",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "227",
            "cityName": "Moga",
            "stateName": "Punjab"
        },
        {
            "_id": "228",
            "cityName": "Proddatur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "229",
            "cityName": "Srinagar",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "230",
            "cityName": "Medinipur",
            "stateName": "West Bengal"
        },
        {
            "_id": "231",
            "cityName": "Habra",
            "stateName": "West Bengal"
        },
        {
            "_id": "232",
            "cityName": "Sasaram",
            "stateName": "Bihar"
        },
        {
            "_id": "233",
            "cityName": "Hajipur",
            "stateName": "Bihar"
        },
        {
            "_id": "234",
            "cityName": "Bhuj",
            "stateName": "Gujarat"
        },
        {
            "_id": "235",
            "cityName": "Shivpuri",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "236",
            "cityName": "Ranaghat",
            "stateName": "West Bengal"
        },
        {
            "_id": "237",
            "cityName": "Shimla",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "238",
            "cityName": "Tiruvannamalai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "239",
            "cityName": "Kaithal",
            "stateName": "Haryana"
        },
        {
            "_id": "240",
            "cityName": "Rajnandgaon",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "241",
            "cityName": "Godhra",
            "stateName": "Gujarat"
        },
        {
            "_id": "242",
            "cityName": "Hazaribag",
            "stateName": "Jharkhand"
        },
        {
            "_id": "243",
            "cityName": "Bhimavaram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "244",
            "cityName": "Mandsaur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "245",
            "cityName": "Dibrugarh",
            "stateName": "Assam"
        },
        {
            "_id": "246",
            "cityName": "Kolar",
            "stateName": "Karnataka"
        },
        {
            "_id": "247",
            "cityName": "Bankura",
            "stateName": "West Bengal"
        },
        {
            "_id": "248",
            "cityName": "Mandya",
            "stateName": "Karnataka"
        },
        {
            "_id": "249",
            "cityName": "Dehri-on-Sone",
            "stateName": "Bihar"
        },
        {
            "_id": "250",
            "cityName": "Madanapalle",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "251",
            "cityName": "Malerkotla",
            "stateName": "Punjab"
        },
        {
            "_id": "252",
            "cityName": "Lalitpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "253",
            "cityName": "Bettiah",
            "stateName": "Bihar"
        },
        {
            "_id": "254",
            "cityName": "Pollachi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "255",
            "cityName": "Khanna",
            "stateName": "Punjab"
        },
        {
            "_id": "256",
            "cityName": "Neemuch",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "257",
            "cityName": "Palwal",
            "stateName": "Haryana"
        },
        {
            "_id": "258",
            "cityName": "Palanpur",
            "stateName": "Gujarat"
        },
        {
            "_id": "259",
            "cityName": "Guntakal",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "260",
            "cityName": "Nabadwip",
            "stateName": "West Bengal"
        },
        {
            "_id": "261",
            "cityName": "Udupi",
            "stateName": "Karnataka"
        },
        {
            "_id": "262",
            "cityName": "Jagdalpur",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "263",
            "cityName": "Motihari",
            "stateName": "Bihar"
        },
        {
            "_id": "264",
            "cityName": "Pilibhit",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "265",
            "cityName": "Dimapur",
            "stateName": "Nagaland"
        },
        {
            "_id": "266",
            "cityName": "Mohali",
            "stateName": "Punjab"
        },
        {
            "_id": "267",
            "cityName": "Sadulpur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "268",
            "cityName": "Rajapalayam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "269",
            "cityName": "Dharmavaram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "270",
            "cityName": "Kashipur",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "271",
            "cityName": "Sivakasi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "272",
            "cityName": "Darjiling",
            "stateName": "West Bengal"
        },
        {
            "_id": "273",
            "cityName": "Chikkamagaluru",
            "stateName": "Karnataka"
        },
        {
            "_id": "274",
            "cityName": "Gudivada",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "275",
            "cityName": "Baleshwar Town",
            "stateName": "Odisha"
        },
        {
            "_id": "276",
            "cityName": "Mancherial",
            "stateName": "Telangana"
        },
        {
            "_id": "277",
            "cityName": "Srikakulam",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "278",
            "cityName": "Adilabad",
            "stateName": "Telangana"
        },
        {
            "_id": "279",
            "cityName": "Yavatmal",
            "stateName": "Maharashtra"
        },
        {
            "_id": "280",
            "cityName": "Barnala",
            "stateName": "Punjab"
        },
        {
            "_id": "281",
            "cityName": "Nagaon",
            "stateName": "Assam"
        },
        {
            "_id": "282",
            "cityName": "Narasaraopet",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "283",
            "cityName": "Raigarh",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "284",
            "cityName": "Roorkee",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "285",
            "cityName": "Valsad",
            "stateName": "Gujarat"
        },
        {
            "_id": "286",
            "cityName": "Ambikapur",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "287",
            "cityName": "Gir_idih",
            "stateName": "Jharkhand"
        },
        {
            "_id": "288",
            "cityName": "Chandausi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "289",
            "cityName": "Purulia",
            "stateName": "West Bengal"
        },
        {
            "_id": "290",
            "cityName": "Patan",
            "stateName": "Gujarat"
        },
        {
            "_id": "291",
            "cityName": "Bagaha",
            "stateName": "Bihar"
        },
        {
            "_id": "292",
            "cityName": "Hardoi ",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "293",
            "cityName": "Achalpur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "294",
            "cityName": "Osmanabad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "295",
            "cityName": "Deesa",
            "stateName": "Gujarat"
        },
        {
            "_id": "296",
            "cityName": "Nandurbar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "297",
            "cityName": "Azamgarh",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "298",
            "cityName": "Ramgarh",
            "stateName": "Jharkhand"
        },
        {
            "_id": "299",
            "cityName": "Firozpur",
            "stateName": "Punjab"
        },
        {
            "_id": "300",
            "cityName": "Baripada Town",
            "stateName": "Odisha"
        },
        {
            "_id": "301",
            "cityName": "Karwar",
            "stateName": "Karnataka"
        },
        {
            "_id": "302",
            "cityName": "Siwan",
            "stateName": "Bihar"
        },
        {
            "_id": "303",
            "cityName": "Rajampet",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "304",
            "cityName": "Pudukkottai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "305",
            "cityName": "Anantnag",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "306",
            "cityName": "Tadpatri",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "307",
            "cityName": "Satara",
            "stateName": "Maharashtra"
        },
        {
            "_id": "308",
            "cityName": "Bhadrak",
            "stateName": "Odisha"
        },
        {
            "_id": "309",
            "cityName": "Kishanganj",
            "stateName": "Bihar"
        },
        {
            "_id": "310",
            "cityName": "Suryapet",
            "stateName": "Telangana"
        },
        {
            "_id": "311",
            "cityName": "Wardha",
            "stateName": "Maharashtra"
        },
        {
            "_id": "312",
            "cityName": "Ranebennuru",
            "stateName": "Karnataka"
        },
        {
            "_id": "313",
            "cityName": "Amreli",
            "stateName": "Gujarat"
        },
        {
            "_id": "314",
            "cityName": "Neyveli (TS)",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "315",
            "cityName": "Jamalpur",
            "stateName": "Bihar"
        },
        {
            "_id": "316",
            "cityName": "Marmagao",
            "stateName": "Goa"
        },
        {
            "_id": "317",
            "cityName": "Udgir",
            "stateName": "Maharashtra"
        },
        {
            "_id": "318",
            "cityName": "Tadepalligudem",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "319",
            "cityName": "Nagapattinam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "320",
            "cityName": "Buxar",
            "stateName": "Bihar"
        },
        {
            "_id": "321",
            "cityName": "Aurangabad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "322",
            "cityName": "Jehanabad",
            "stateName": "Bihar"
        },
        {
            "_id": "323",
            "cityName": "Phagwara",
            "stateName": "Punjab"
        },
        {
            "_id": "324",
            "cityName": "Khair",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "325",
            "cityName": "Sawai Madhopur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "326",
            "cityName": "Kapurthala",
            "stateName": "Punjab"
        },
        {
            "_id": "327",
            "cityName": "Chilakaluripet",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "328",
            "cityName": "Aurangabad",
            "stateName": "Bihar"
        },
        {
            "_id": "329",
            "cityName": "Malappuram",
            "stateName": "Kerala"
        },
        {
            "_id": "330",
            "cityName": "Rewari",
            "stateName": "Haryana"
        },
        {
            "_id": "331",
            "cityName": "Nagaur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "332",
            "cityName": "Sultanpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "333",
            "cityName": "Nagda",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "334",
            "cityName": "Port Blair",
            "stateName": "Andaman and Nicobar Islands"
        },
        {
            "_id": "335",
            "cityName": "Lakhisarai",
            "stateName": "Bihar"
        },
        {
            "_id": "336",
            "cityName": "Panaji",
            "stateName": "Goa"
        },
        {
            "_id": "337",
            "cityName": "Tinsukia",
            "stateName": "Assam"
        },
        {
            "_id": "338",
            "cityName": "Itarsi",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "339",
            "cityName": "Kohima",
            "stateName": "Nagaland"
        },
        {
            "_id": "340",
            "cityName": "Balangir",
            "stateName": "Odisha"
        },
        {
            "_id": "341",
            "cityName": "Nawada",
            "stateName": "Bihar"
        },
        {
            "_id": "342",
            "cityName": "Jharsuguda",
            "stateName": "Odisha"
        },
        {
            "_id": "343",
            "cityName": "Jagtial",
            "stateName": "Telangana"
        },
        {
            "_id": "344",
            "cityName": "Viluppuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "345",
            "cityName": "Amalner",
            "stateName": "Maharashtra"
        },
        {
            "_id": "346",
            "cityName": "Zirakpur",
            "stateName": "Punjab"
        },
        {
            "_id": "347",
            "cityName": "Tanda",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "348",
            "cityName": "Tiruchengode",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "349",
            "cityName": "Nagina",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "350",
            "cityName": "Yemmiganur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "351",
            "cityName": "Vaniyambadi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "352",
            "cityName": "Sarni",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "353",
            "cityName": "Theni Allinagaram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "354",
            "cityName": "Margao",
            "stateName": "Goa"
        },
        {
            "_id": "355",
            "cityName": "Akot",
            "stateName": "Maharashtra"
        },
        {
            "_id": "356",
            "cityName": "Sehore",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "357",
            "cityName": "Mhow Cantonment",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "358",
            "cityName": "Kot Kapura",
            "stateName": "Punjab"
        },
        {
            "_id": "359",
            "cityName": "Makrana",
            "stateName": "Rajasthan"
        },
        {
            "_id": "360",
            "cityName": "Pandharpur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "361",
            "cityName": "Miryalaguda",
            "stateName": "Telangana"
        },
        {
            "_id": "362",
            "cityName": "Shamli",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "363",
            "cityName": "Seoni",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "364",
            "cityName": "Ranibennur",
            "stateName": "Karnataka"
        },
        {
            "_id": "365",
            "cityName": "Kadiri",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "366",
            "cityName": "Shrirampur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "367",
            "cityName": "Rudrapur",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "368",
            "cityName": "Parli",
            "stateName": "Maharashtra"
        },
        {
            "_id": "369",
            "cityName": "Najibabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "370",
            "cityName": "Nirmal",
            "stateName": "Telangana"
        },
        {
            "_id": "371",
            "cityName": "Udhagamandalam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "372",
            "cityName": "Shikohabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "373",
            "cityName": "Jhumri Tilaiya",
            "stateName": "Jharkhand"
        },
        {
            "_id": "374",
            "cityName": "Aruppukkottai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "375",
            "cityName": "Ponnani",
            "stateName": "Kerala"
        },
        {
            "_id": "376",
            "cityName": "Jamui",
            "stateName": "Bihar"
        },
        {
            "_id": "377",
            "cityName": "Sitamarhi",
            "stateName": "Bihar"
        },
        {
            "_id": "378",
            "cityName": "Chirala",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "379",
            "cityName": "Anjar",
            "stateName": "Gujarat"
        },
        {
            "_id": "380",
            "cityName": "Karaikal",
            "stateName": "Puducherry"
        },
        {
            "_id": "381",
            "cityName": "Hansi",
            "stateName": "Haryana"
        },
        {
            "_id": "382",
            "cityName": "Anakapalle",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "383",
            "cityName": "Mahasamund",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "384",
            "cityName": "Far_idkot",
            "stateName": "Punjab"
        },
        {
            "_id": "385",
            "cityName": "Saunda",
            "stateName": "Jharkhand"
        },
        {
            "_id": "386",
            "cityName": "Dhoraji",
            "stateName": "Gujarat"
        },
        {
            "_id": "387",
            "cityName": "Paramakudi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "388",
            "cityName": "Balaghat",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "389",
            "cityName": "Sujangarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "390",
            "cityName": "Khambhat",
            "stateName": "Gujarat"
        },
        {
            "_id": "391",
            "cityName": "Muktsar",
            "stateName": "Punjab"
        },
        {
            "_id": "392",
            "cityName": "Rajpura",
            "stateName": "Punjab"
        },
        {
            "_id": "393",
            "cityName": "Kavali",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "394",
            "cityName": "Dhamtari",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "395",
            "cityName": "Ashok Nagar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "396",
            "cityName": "Sardarshahar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "397",
            "cityName": "Mahuva",
            "stateName": "Gujarat"
        },
        {
            "_id": "398",
            "cityName": "Bargarh",
            "stateName": "Odisha"
        },
        {
            "_id": "399",
            "cityName": "Kamareddy",
            "stateName": "Telangana"
        },
        {
            "_id": "400",
            "cityName": "Sahibganj",
            "stateName": "Jharkhand"
        },
        {
            "_id": "401",
            "cityName": "Kothagudem",
            "stateName": "Telangana"
        },
        {
            "_id": "402",
            "cityName": "Ramanagaram",
            "stateName": "Karnataka"
        },
        {
            "_id": "403",
            "cityName": "Gokak",
            "stateName": "Karnataka"
        },
        {
            "_id": "404",
            "cityName": "Tikamgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "405",
            "cityName": "Araria",
            "stateName": "Bihar"
        },
        {
            "_id": "406",
            "cityName": "Rishikesh",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "407",
            "cityName": "Shahdol",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "408",
            "cityName": "Medininagar (Daltonganj)",
            "stateName": "Jharkhand"
        },
        {
            "_id": "409",
            "cityName": "Arakkonam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "410",
            "cityName": "Washim",
            "stateName": "Maharashtra"
        },
        {
            "_id": "411",
            "cityName": "Sangrur",
            "stateName": "Punjab"
        },
        {
            "_id": "412",
            "cityName": "Bodhan",
            "stateName": "Telangana"
        },
        {
            "_id": "413",
            "cityName": "Fazilka",
            "stateName": "Punjab"
        },
        {
            "_id": "414",
            "cityName": "Palacole",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "415",
            "cityName": "Keshod",
            "stateName": "Gujarat"
        },
        {
            "_id": "416",
            "cityName": "Sullurpeta",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "417",
            "cityName": "Wadhwan",
            "stateName": "Gujarat"
        },
        {
            "_id": "418",
            "cityName": "Gurdaspur",
            "stateName": "Punjab"
        },
        {
            "_id": "419",
            "cityName": "Vatakara",
            "stateName": "Kerala"
        },
        {
            "_id": "420",
            "cityName": "Tura",
            "stateName": "Meghalaya"
        },
        {
            "_id": "421",
            "cityName": "Narnaul",
            "stateName": "Haryana"
        },
        {
            "_id": "422",
            "cityName": "Kharar",
            "stateName": "Punjab"
        },
        {
            "_id": "423",
            "cityName": "Yadgir",
            "stateName": "Karnataka"
        },
        {
            "_id": "424",
            "cityName": "Ambejogai",
            "stateName": "Maharashtra"
        },
        {
            "_id": "425",
            "cityName": "Ankleshwar",
            "stateName": "Gujarat"
        },
        {
            "_id": "426",
            "cityName": "Savarkundla",
            "stateName": "Gujarat"
        },
        {
            "_id": "427",
            "cityName": "Paradip",
            "stateName": "Odisha"
        },
        {
            "_id": "428",
            "cityName": "Virudhachalam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "429",
            "cityName": "Kanhangad",
            "stateName": "Kerala"
        },
        {
            "_id": "430",
            "cityName": "Kadi",
            "stateName": "Gujarat"
        },
        {
            "_id": "431",
            "cityName": "Srivilliputhur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "432",
            "cityName": "Gobindgarh",
            "stateName": "Punjab"
        },
        {
            "_id": "433",
            "cityName": "Tindivanam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "434",
            "cityName": "Mansa",
            "stateName": "Punjab"
        },
        {
            "_id": "435",
            "cityName": "Taliparamba",
            "stateName": "Kerala"
        },
        {
            "_id": "436",
            "cityName": "Manmad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "437",
            "cityName": "Tanuku",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "438",
            "cityName": "Rayachoti",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "439",
            "cityName": "Virudhunagar",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "440",
            "cityName": "Koyilandy",
            "stateName": "Kerala"
        },
        {
            "_id": "441",
            "cityName": "Jorhat",
            "stateName": "Assam"
        },
        {
            "_id": "442",
            "cityName": "Karur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "443",
            "cityName": "Valparai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "444",
            "cityName": "Srikalahasti",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "445",
            "cityName": "Neyyattinkara",
            "stateName": "Kerala"
        },
        {
            "_id": "446",
            "cityName": "Bapatla",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "447",
            "cityName": "Fatehabad",
            "stateName": "Haryana"
        },
        {
            "_id": "448",
            "cityName": "Malout",
            "stateName": "Punjab"
        },
        {
            "_id": "449",
            "cityName": "Sankarankovil",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "450",
            "cityName": "Tenkasi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "451",
            "cityName": "Ratnagiri",
            "stateName": "Maharashtra"
        },
        {
            "_id": "452",
            "cityName": "Rabkavi Banhatti",
            "stateName": "Karnataka"
        },
        {
            "_id": "453",
            "cityName": "Sikandrabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "454",
            "cityName": "Chaibasa",
            "stateName": "Jharkhand"
        },
        {
            "_id": "455",
            "cityName": "Chirmiri",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "456",
            "cityName": "Palwancha",
            "stateName": "Telangana"
        },
        {
            "_id": "457",
            "cityName": "Bhawanipatna",
            "stateName": "Odisha"
        },
        {
            "_id": "458",
            "cityName": "Kayamkulam",
            "stateName": "Kerala"
        },
        {
            "_id": "459",
            "cityName": "Pithampur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "460",
            "cityName": "Nabha",
            "stateName": "Punjab"
        },
        {
            "_id": "461",
            "cityName": "Shahabad, Hardoi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "462",
            "cityName": "Dhenkanal",
            "stateName": "Odisha"
        },
        {
            "_id": "463",
            "cityName": "Uran Islampur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "464",
            "cityName": "Gopalganj",
            "stateName": "Bihar"
        },
        {
            "_id": "465",
            "cityName": "Bongaigaon City",
            "stateName": "Assam"
        },
        {
            "_id": "466",
            "cityName": "Palani",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "467",
            "cityName": "Pusad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "468",
            "cityName": "Sopore",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "469",
            "cityName": "Pilkhuwa",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "470",
            "cityName": "Tarn Taran",
            "stateName": "Punjab"
        },
        {
            "_id": "471",
            "cityName": "Renukoot",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "472",
            "cityName": "Mandamarri",
            "stateName": "Telangana"
        },
        {
            "_id": "473",
            "cityName": "Shahabad",
            "stateName": "Karnataka"
        },
        {
            "_id": "474",
            "cityName": "Barbil",
            "stateName": "Odisha"
        },
        {
            "_id": "475",
            "cityName": "Koratla",
            "stateName": "Telangana"
        },
        {
            "_id": "476",
            "cityName": "Madhubani",
            "stateName": "Bihar"
        },
        {
            "_id": "477",
            "cityName": "Arambagh",
            "stateName": "West Bengal"
        },
        {
            "_id": "478",
            "cityName": "Gohana",
            "stateName": "Haryana"
        },
        {
            "_id": "479",
            "cityName": "Ladnu",
            "stateName": "Rajasthan"
        },
        {
            "_id": "480",
            "cityName": "Pattukkottai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "481",
            "cityName": "Sirsi",
            "stateName": "Karnataka"
        },
        {
            "_id": "482",
            "cityName": "Sircilla",
            "stateName": "Telangana"
        },
        {
            "_id": "483",
            "cityName": "Tamluk",
            "stateName": "West Bengal"
        },
        {
            "_id": "484",
            "cityName": "Jagraon",
            "stateName": "Punjab"
        },
        {
            "_id": "485",
            "cityName": "AlipurdUrban Agglomerationr",
            "stateName": "West Bengal"
        },
        {
            "_id": "486",
            "cityName": "Alirajpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "487",
            "cityName": "Tandur",
            "stateName": "Telangana"
        },
        {
            "_id": "488",
            "cityName": "Na_idupet",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "489",
            "cityName": "Tirupathur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "490",
            "cityName": "Tohana",
            "stateName": "Haryana"
        },
        {
            "_id": "491",
            "cityName": "Ratangarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "492",
            "cityName": "Dhubri",
            "stateName": "Assam"
        },
        {
            "_id": "493",
            "cityName": "Masaurhi",
            "stateName": "Bihar"
        },
        {
            "_id": "494",
            "cityName": "Visnagar",
            "stateName": "Gujarat"
        },
        {
            "_id": "495",
            "cityName": "Vrindavan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "496",
            "cityName": "Nokha",
            "stateName": "Rajasthan"
        },
        {
            "_id": "497",
            "cityName": "Nagari",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "498",
            "cityName": "Narwana",
            "stateName": "Haryana"
        },
        {
            "_id": "499",
            "cityName": "Ramanathapuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "500",
            "cityName": "Ujhani",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "501",
            "cityName": "Samastipur",
            "stateName": "Bihar"
        },
        {
            "_id": "502",
            "cityName": "Laharpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "503",
            "cityName": "Sangamner",
            "stateName": "Maharashtra"
        },
        {
            "_id": "504",
            "cityName": "Nimbahera",
            "stateName": "Rajasthan"
        },
        {
            "_id": "505",
            "cityName": "S_iddipet",
            "stateName": "Telangana"
        },
        {
            "_id": "506",
            "cityName": "Suri",
            "stateName": "West Bengal"
        },
        {
            "_id": "507",
            "cityName": "Diphu",
            "stateName": "Assam"
        },
        {
            "_id": "508",
            "cityName": "Jhargram",
            "stateName": "West Bengal"
        },
        {
            "_id": "509",
            "cityName": "Shirpur-Warwade",
            "stateName": "Maharashtra"
        },
        {
            "_id": "510",
            "cityName": "Tilhar",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "511",
            "cityName": "Sindhnur",
            "stateName": "Karnataka"
        },
        {
            "_id": "512",
            "cityName": "Udumalaipettai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "513",
            "cityName": "Malkapur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "514",
            "cityName": "Wanaparthy",
            "stateName": "Telangana"
        },
        {
            "_id": "515",
            "cityName": "Gudur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "516",
            "cityName": "Kendujhar",
            "stateName": "Odisha"
        },
        {
            "_id": "517",
            "cityName": "Mandla",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "518",
            "cityName": "Mandi",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "519",
            "cityName": "Nedumangad",
            "stateName": "Kerala"
        },
        {
            "_id": "520",
            "cityName": "North Lakhimpur",
            "stateName": "Assam"
        },
        {
            "_id": "521",
            "cityName": "Vinukonda",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "522",
            "cityName": "Tiptur",
            "stateName": "Karnataka"
        },
        {
            "_id": "523",
            "cityName": "Gobichettipalayam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "524",
            "cityName": "Sunabeda",
            "stateName": "Odisha"
        },
        {
            "_id": "525",
            "cityName": "Wani",
            "stateName": "Maharashtra"
        },
        {
            "_id": "526",
            "cityName": "Upleta",
            "stateName": "Gujarat"
        },
        {
            "_id": "527",
            "cityName": "Narasapuram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "528",
            "cityName": "Nuzv_id",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "529",
            "cityName": "Tezpur",
            "stateName": "Assam"
        },
        {
            "_id": "530",
            "cityName": "Una",
            "stateName": "Gujarat"
        },
        {
            "_id": "531",
            "cityName": "Markapur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "532",
            "cityName": "Sheopur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "533",
            "cityName": "Thiruvarur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "534",
            "cityName": "S_idhpur",
            "stateName": "Gujarat"
        },
        {
            "_id": "535",
            "cityName": "Sahaswan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "536",
            "cityName": "Suratgarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "537",
            "cityName": "Shajapur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "538",
            "cityName": "Rayagada",
            "stateName": "Odisha"
        },
        {
            "_id": "539",
            "cityName": "Lonavla",
            "stateName": "Maharashtra"
        },
        {
            "_id": "540",
            "cityName": "Ponnur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "541",
            "cityName": "Kagaznagar",
            "stateName": "Telangana"
        },
        {
            "_id": "542",
            "cityName": "Gadwal",
            "stateName": "Telangana"
        },
        {
            "_id": "543",
            "cityName": "Bhatapara",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "544",
            "cityName": "Kandukur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "545",
            "cityName": "Sangareddy",
            "stateName": "Telangana"
        },
        {
            "_id": "546",
            "cityName": "Unjha",
            "stateName": "Gujarat"
        },
        {
            "_id": "547",
            "cityName": "Lunglei",
            "stateName": "Mizoram"
        },
        {
            "_id": "548",
            "cityName": "Karimganj",
            "stateName": "Assam"
        },
        {
            "_id": "549",
            "cityName": "Kannur",
            "stateName": "Kerala"
        },
        {
            "_id": "550",
            "cityName": "Bobbili",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "551",
            "cityName": "Mokameh",
            "stateName": "Bihar"
        },
        {
            "_id": "552",
            "cityName": "Talegaon Dabhade",
            "stateName": "Maharashtra"
        },
        {
            "_id": "553",
            "cityName": "Anjangaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "554",
            "cityName": "Mangrol",
            "stateName": "Gujarat"
        },
        {
            "_id": "555",
            "cityName": "Sunam",
            "stateName": "Punjab"
        },
        {
            "_id": "556",
            "cityName": "Gangarampur",
            "stateName": "West Bengal"
        },
        {
            "_id": "557",
            "cityName": "Thiruvallur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "558",
            "cityName": "Tirur",
            "stateName": "Kerala"
        },
        {
            "_id": "559",
            "cityName": "Rath",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "560",
            "cityName": "Jatani",
            "stateName": "Odisha"
        },
        {
            "_id": "561",
            "cityName": "Viramgam",
            "stateName": "Gujarat"
        },
        {
            "_id": "562",
            "cityName": "Rajsamand",
            "stateName": "Rajasthan"
        },
        {
            "_id": "563",
            "cityName": "Yanam",
            "stateName": "Puducherry"
        },
        {
            "_id": "564",
            "cityName": "Kottayam",
            "stateName": "Kerala"
        },
        {
            "_id": "565",
            "cityName": "Panruti",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "566",
            "cityName": "Dhuri",
            "stateName": "Punjab"
        },
        {
            "_id": "567",
            "cityName": "Namakkal",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "568",
            "cityName": "Kasaragod",
            "stateName": "Kerala"
        },
        {
            "_id": "569",
            "cityName": "Modasa",
            "stateName": "Gujarat"
        },
        {
            "_id": "570",
            "cityName": "Rayadurg",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "571",
            "cityName": "Supaul",
            "stateName": "Bihar"
        },
        {
            "_id": "572",
            "cityName": "Kunnamkulam",
            "stateName": "Kerala"
        },
        {
            "_id": "573",
            "cityName": "Umred",
            "stateName": "Maharashtra"
        },
        {
            "_id": "574",
            "cityName": "Bellampalle",
            "stateName": "Telangana"
        },
        {
            "_id": "575",
            "cityName": "Sibsagar",
            "stateName": "Assam"
        },
        {
            "_id": "576",
            "cityName": "Mandi Dabwali",
            "stateName": "Haryana"
        },
        {
            "_id": "577",
            "cityName": "Ottappalam",
            "stateName": "Kerala"
        },
        {
            "_id": "578",
            "cityName": "Dumraon",
            "stateName": "Bihar"
        },
        {
            "_id": "579",
            "cityName": "Samalkot",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "580",
            "cityName": "Jaggaiahpet",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "581",
            "cityName": "Goalpara",
            "stateName": "Assam"
        },
        {
            "_id": "582",
            "cityName": "Tuni",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "583",
            "cityName": "Lachhmangarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "584",
            "cityName": "Bhongir",
            "stateName": "Telangana"
        },
        {
            "_id": "585",
            "cityName": "Amalapuram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "586",
            "cityName": "Firozpur Cantt.",
            "stateName": "Punjab"
        },
        {
            "_id": "587",
            "cityName": "Vikarabad",
            "stateName": "Telangana"
        },
        {
            "_id": "588",
            "cityName": "Thiruvalla",
            "stateName": "Kerala"
        },
        {
            "_id": "589",
            "cityName": "Sherkot",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "590",
            "cityName": "Palghar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "591",
            "cityName": "Shegaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "592",
            "cityName": "Jangaon",
            "stateName": "Telangana"
        },
        {
            "_id": "593",
            "cityName": "Bheemunipatnam",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "594",
            "cityName": "Panna",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "595",
            "cityName": "Thodupuzha",
            "stateName": "Kerala"
        },
        {
            "_id": "596",
            "cityName": "KathUrban Agglomeration",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "597",
            "cityName": "Palitana",
            "stateName": "Gujarat"
        },
        {
            "_id": "598",
            "cityName": "Arwal",
            "stateName": "Bihar"
        },
        {
            "_id": "599",
            "cityName": "Venkatagiri",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "600",
            "cityName": "Kalpi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "601",
            "cityName": "Rajgarh (Churu)",
            "stateName": "Rajasthan"
        },
        {
            "_id": "602",
            "cityName": "Sattenapalle",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "603",
            "cityName": "Arsikere",
            "stateName": "Karnataka"
        },
        {
            "_id": "604",
            "cityName": "Ozar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "605",
            "cityName": "Thirumangalam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "606",
            "cityName": "Petlad",
            "stateName": "Gujarat"
        },
        {
            "_id": "607",
            "cityName": "Nasirabad",
            "stateName": "Rajasthan"
        },
        {
            "_id": "608",
            "cityName": "Phaltan",
            "stateName": "Maharashtra"
        },
        {
            "_id": "609",
            "cityName": "Rampurhat",
            "stateName": "West Bengal"
        },
        {
            "_id": "610",
            "cityName": "Nanjangud",
            "stateName": "Karnataka"
        },
        {
            "_id": "611",
            "cityName": "Forbesganj",
            "stateName": "Bihar"
        },
        {
            "_id": "612",
            "cityName": "Tundla",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "613",
            "cityName": "BhabUrban Agglomeration",
            "stateName": "Bihar"
        },
        {
            "_id": "614",
            "cityName": "Sagara",
            "stateName": "Karnataka"
        },
        {
            "_id": "615",
            "cityName": "Pithapuram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "616",
            "cityName": "Sira",
            "stateName": "Karnataka"
        },
        {
            "_id": "617",
            "cityName": "Bhadrachalam",
            "stateName": "Telangana"
        },
        {
            "_id": "618",
            "cityName": "Charkhi Dadri",
            "stateName": "Haryana"
        },
        {
            "_id": "619",
            "cityName": "Chatra",
            "stateName": "Jharkhand"
        },
        {
            "_id": "620",
            "cityName": "Palasa Kasibugga",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "621",
            "cityName": "Nohar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "622",
            "cityName": "Yevla",
            "stateName": "Maharashtra"
        },
        {
            "_id": "623",
            "cityName": "Sirhind Fatehgarh Sahib",
            "stateName": "Punjab"
        },
        {
            "_id": "624",
            "cityName": "Bhainsa",
            "stateName": "Telangana"
        },
        {
            "_id": "625",
            "cityName": "Parvathipuram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "626",
            "cityName": "Shahade",
            "stateName": "Maharashtra"
        },
        {
            "_id": "627",
            "cityName": "Chalakudy",
            "stateName": "Kerala"
        },
        {
            "_id": "628",
            "cityName": "Narkatiaganj",
            "stateName": "Bihar"
        },
        {
            "_id": "629",
            "cityName": "Kapadvanj",
            "stateName": "Gujarat"
        },
        {
            "_id": "630",
            "cityName": "Macherla",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "631",
            "cityName": "Raghogarh-Vijaypur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "632",
            "cityName": "Rupnagar",
            "stateName": "Punjab"
        },
        {
            "_id": "633",
            "cityName": "Naugachhia",
            "stateName": "Bihar"
        },
        {
            "_id": "634",
            "cityName": "Sendhwa",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "635",
            "cityName": "Byasanagar",
            "stateName": "Odisha"
        },
        {
            "_id": "636",
            "cityName": "Sandila",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "637",
            "cityName": "Gooty",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "638",
            "cityName": "Salur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "639",
            "cityName": "Nanpara",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "640",
            "cityName": "Sardhana",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "641",
            "cityName": "Vita",
            "stateName": "Maharashtra"
        },
        {
            "_id": "642",
            "cityName": "Gumia",
            "stateName": "Jharkhand"
        },
        {
            "_id": "643",
            "cityName": "Puttur",
            "stateName": "Karnataka"
        },
        {
            "_id": "644",
            "cityName": "Jalandhar Cantt.",
            "stateName": "Punjab"
        },
        {
            "_id": "645",
            "cityName": "Nehtaur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "646",
            "cityName": "Changanassery",
            "stateName": "Kerala"
        },
        {
            "_id": "647",
            "cityName": "Mandapeta",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "648",
            "cityName": "Dumka",
            "stateName": "Jharkhand"
        },
        {
            "_id": "649",
            "cityName": "Seohara",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "650",
            "cityName": "Umarkhed",
            "stateName": "Maharashtra"
        },
        {
            "_id": "651",
            "cityName": "Madhupur",
            "stateName": "Jharkhand"
        },
        {
            "_id": "652",
            "cityName": "Vikramasingapuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "653",
            "cityName": "Punalur",
            "stateName": "Kerala"
        },
        {
            "_id": "654",
            "cityName": "Kendrapara",
            "stateName": "Odisha"
        },
        {
            "_id": "655",
            "cityName": "Sihor",
            "stateName": "Gujarat"
        },
        {
            "_id": "656",
            "cityName": "Nellikuppam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "657",
            "cityName": "Samana",
            "stateName": "Punjab"
        },
        {
            "_id": "658",
            "cityName": "Warora",
            "stateName": "Maharashtra"
        },
        {
            "_id": "659",
            "cityName": "Nilambur",
            "stateName": "Kerala"
        },
        {
            "_id": "660",
            "cityName": "Rasipuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "661",
            "cityName": "Ramnagar",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "662",
            "cityName": "Jammalamadugu",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "663",
            "cityName": "Nawanshahr",
            "stateName": "Punjab"
        },
        {
            "_id": "664",
            "cityName": "Thoubal",
            "stateName": "Manipur"
        },
        {
            "_id": "665",
            "cityName": "Athni",
            "stateName": "Karnataka"
        },
        {
            "_id": "666",
            "cityName": "Cherthala",
            "stateName": "Kerala"
        },
        {
            "_id": "667",
            "cityName": "S_idhi",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "668",
            "cityName": "Farooqnagar",
            "stateName": "Telangana"
        },
        {
            "_id": "669",
            "cityName": "Peddapuram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "670",
            "cityName": "Chirkunda",
            "stateName": "Jharkhand"
        },
        {
            "_id": "671",
            "cityName": "Pachora",
            "stateName": "Maharashtra"
        },
        {
            "_id": "672",
            "cityName": "Madhepura",
            "stateName": "Bihar"
        },
        {
            "_id": "673",
            "cityName": "Pithoragarh",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "674",
            "cityName": "Tumsar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "675",
            "cityName": "Phalodi",
            "stateName": "Rajasthan"
        },
        {
            "_id": "676",
            "cityName": "Tiruttani",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "677",
            "cityName": "Rampura Phul",
            "stateName": "Punjab"
        },
        {
            "_id": "678",
            "cityName": "Perinthalmanna",
            "stateName": "Kerala"
        },
        {
            "_id": "679",
            "cityName": "Padrauna",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "680",
            "cityName": "Pipariya",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "681",
            "cityName": "Dalli-Rajhara",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "682",
            "cityName": "Punganur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "683",
            "cityName": "Mattannur",
            "stateName": "Kerala"
        },
        {
            "_id": "684",
            "cityName": "Mathura",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "685",
            "cityName": "Thakurdwara",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "686",
            "cityName": "Nandivaram-Guduvancheri",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "687",
            "cityName": "Mulbagal",
            "stateName": "Karnataka"
        },
        {
            "_id": "688",
            "cityName": "Manjlegaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "689",
            "cityName": "Wankaner",
            "stateName": "Gujarat"
        },
        {
            "_id": "690",
            "cityName": "Sillod",
            "stateName": "Maharashtra"
        },
        {
            "_id": "691",
            "cityName": "N_idadavole",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "692",
            "cityName": "Surapura",
            "stateName": "Karnataka"
        },
        {
            "_id": "693",
            "cityName": "Rajagangapur",
            "stateName": "Odisha"
        },
        {
            "_id": "694",
            "cityName": "Sheikhpura",
            "stateName": "Bihar"
        },
        {
            "_id": "695",
            "cityName": "Parlakhemundi",
            "stateName": "Odisha"
        },
        {
            "_id": "696",
            "cityName": "Kalimpong",
            "stateName": "West Bengal"
        },
        {
            "_id": "697",
            "cityName": "Siruguppa",
            "stateName": "Karnataka"
        },
        {
            "_id": "698",
            "cityName": "Arvi",
            "stateName": "Maharashtra"
        },
        {
            "_id": "699",
            "cityName": "Limbdi",
            "stateName": "Gujarat"
        },
        {
            "_id": "700",
            "cityName": "Barpeta",
            "stateName": "Assam"
        },
        {
            "_id": "701",
            "cityName": "Manglaur",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "702",
            "cityName": "Repalle",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "703",
            "cityName": "Mudhol",
            "stateName": "Karnataka"
        },
        {
            "_id": "704",
            "cityName": "Shujalpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "705",
            "cityName": "Mandvi",
            "stateName": "Gujarat"
        },
        {
            "_id": "706",
            "cityName": "Thangadh",
            "stateName": "Gujarat"
        },
        {
            "_id": "707",
            "cityName": "Sironj",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "708",
            "cityName": "Nandura",
            "stateName": "Maharashtra"
        },
        {
            "_id": "709",
            "cityName": "Shoranur",
            "stateName": "Kerala"
        },
        {
            "_id": "710",
            "cityName": "Nathdwara",
            "stateName": "Rajasthan"
        },
        {
            "_id": "711",
            "cityName": "Periyakulam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "712",
            "cityName": "Sultanganj",
            "stateName": "Bihar"
        },
        {
            "_id": "713",
            "cityName": "Medak",
            "stateName": "Telangana"
        },
        {
            "_id": "714",
            "cityName": "Narayanpet",
            "stateName": "Telangana"
        },
        {
            "_id": "715",
            "cityName": "Raxaul Bazar",
            "stateName": "Bihar"
        },
        {
            "_id": "716",
            "cityName": "Rajauri",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "717",
            "cityName": "Pernampattu",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "718",
            "cityName": "Nainital",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "719",
            "cityName": "Ramachandrapuram",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "720",
            "cityName": "Vaijapur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "721",
            "cityName": "Nangal",
            "stateName": "Punjab"
        },
        {
            "_id": "722",
            "cityName": "S_idlaghatta",
            "stateName": "Karnataka"
        },
        {
            "_id": "723",
            "cityName": "Punch",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "724",
            "cityName": "Pandhurna",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "725",
            "cityName": "Wadgaon Road",
            "stateName": "Maharashtra"
        },
        {
            "_id": "726",
            "cityName": "Talcher",
            "stateName": "Odisha"
        },
        {
            "_id": "727",
            "cityName": "Varkala",
            "stateName": "Kerala"
        },
        {
            "_id": "728",
            "cityName": "Pilani",
            "stateName": "Rajasthan"
        },
        {
            "_id": "729",
            "cityName": "Nowgong",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "730",
            "cityName": "Naila Janjgir",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "731",
            "cityName": "Mapusa",
            "stateName": "Goa"
        },
        {
            "_id": "732",
            "cityName": "Vellakoil",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "733",
            "cityName": "Merta City",
            "stateName": "Rajasthan"
        },
        {
            "_id": "734",
            "cityName": "Sivaganga",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "735",
            "cityName": "Mand_ideep",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "736",
            "cityName": "Sailu",
            "stateName": "Maharashtra"
        },
        {
            "_id": "737",
            "cityName": "Vyara",
            "stateName": "Gujarat"
        },
        {
            "_id": "738",
            "cityName": "Kovvur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "739",
            "cityName": "Vadalur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "740",
            "cityName": "Nawabganj",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "741",
            "cityName": "Padra",
            "stateName": "Gujarat"
        },
        {
            "_id": "742",
            "cityName": "Sainthia",
            "stateName": "West Bengal"
        },
        {
            "_id": "743",
            "cityName": "Siana",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "744",
            "cityName": "Shahpur",
            "stateName": "Karnataka"
        },
        {
            "_id": "745",
            "cityName": "Sojat",
            "stateName": "Rajasthan"
        },
        {
            "_id": "746",
            "cityName": "Noorpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "747",
            "cityName": "Paravoor",
            "stateName": "Kerala"
        },
        {
            "_id": "748",
            "cityName": "Murtijapur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "749",
            "cityName": "Ramnagar",
            "stateName": "Bihar"
        },
        {
            "_id": "750",
            "cityName": "Sundargarh",
            "stateName": "Odisha"
        },
        {
            "_id": "751",
            "cityName": "Taki",
            "stateName": "West Bengal"
        },
        {
            "_id": "752",
            "cityName": "Saundatti-Yellamma",
            "stateName": "Karnataka"
        },
        {
            "_id": "753",
            "cityName": "Pathanamthitta",
            "stateName": "Kerala"
        },
        {
            "_id": "754",
            "cityName": "Wadi",
            "stateName": "Karnataka"
        },
        {
            "_id": "755",
            "cityName": "Rameshwaram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "756",
            "cityName": "Tasgaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "757",
            "cityName": "Sikandra Rao",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "758",
            "cityName": "Sihora",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "759",
            "cityName": "Tiruvethipuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "760",
            "cityName": "Tiruvuru",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "761",
            "cityName": "Mehkar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "762",
            "cityName": "Peringathur",
            "stateName": "Kerala"
        },
        {
            "_id": "763",
            "cityName": "Perambalur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "764",
            "cityName": "Manvi",
            "stateName": "Karnataka"
        },
        {
            "_id": "765",
            "cityName": "Zunheboto",
            "stateName": "Nagaland"
        },
        {
            "_id": "766",
            "cityName": "Mahnar Bazar",
            "stateName": "Bihar"
        },
        {
            "_id": "767",
            "cityName": "Attingal",
            "stateName": "Kerala"
        },
        {
            "_id": "768",
            "cityName": "Shahbad",
            "stateName": "Haryana"
        },
        {
            "_id": "769",
            "cityName": "Puranpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "770",
            "cityName": "Nelamangala",
            "stateName": "Karnataka"
        },
        {
            "_id": "771",
            "cityName": "Nakodar",
            "stateName": "Punjab"
        },
        {
            "_id": "772",
            "cityName": "Lunawada",
            "stateName": "Gujarat"
        },
        {
            "_id": "773",
            "cityName": "Mursh_idabad",
            "stateName": "West Bengal"
        },
        {
            "_id": "774",
            "cityName": "Mahe",
            "stateName": "Puducherry"
        },
        {
            "_id": "775",
            "cityName": "Lanka",
            "stateName": "Assam"
        },
        {
            "_id": "776",
            "cityName": "Rudauli",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "777",
            "cityName": "Tuensang",
            "stateName": "Nagaland"
        },
        {
            "_id": "778",
            "cityName": "Lakshmeshwar",
            "stateName": "Karnataka"
        },
        {
            "_id": "779",
            "cityName": "Zira",
            "stateName": "Punjab"
        },
        {
            "_id": "780",
            "cityName": "Yawal",
            "stateName": "Maharashtra"
        },
        {
            "_id": "781",
            "cityName": "Thana Bhawan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "782",
            "cityName": "Ramdurg",
            "stateName": "Karnataka"
        },
        {
            "_id": "783",
            "cityName": "Pulgaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "784",
            "cityName": "Sadasivpet",
            "stateName": "Telangana"
        },
        {
            "_id": "785",
            "cityName": "Nargund",
            "stateName": "Karnataka"
        },
        {
            "_id": "786",
            "cityName": "Neem-Ka-Thana",
            "stateName": "Rajasthan"
        },
        {
            "_id": "787",
            "cityName": "Memari",
            "stateName": "West Bengal"
        },
        {
            "_id": "788",
            "cityName": "Nilanga",
            "stateName": "Maharashtra"
        },
        {
            "_id": "789",
            "cityName": "Naharlagun",
            "stateName": "Arunachal Pradesh"
        },
        {
            "_id": "790",
            "cityName": "Pakaur",
            "stateName": "Jharkhand"
        },
        {
            "_id": "791",
            "cityName": "Wai",
            "stateName": "Maharashtra"
        },
        {
            "_id": "792",
            "cityName": "Tarikere",
            "stateName": "Karnataka"
        },
        {
            "_id": "793",
            "cityName": "Malavalli",
            "stateName": "Karnataka"
        },
        {
            "_id": "794",
            "cityName": "Raisen",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "795",
            "cityName": "Lahar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "796",
            "cityName": "Uravakonda",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "797",
            "cityName": "Savanur",
            "stateName": "Karnataka"
        },
        {
            "_id": "798",
            "cityName": "Sirohi",
            "stateName": "Rajasthan"
        },
        {
            "_id": "799",
            "cityName": "Udhampur",
            "stateName": "Jammu and Kashmir"
        },
        {
            "_id": "800",
            "cityName": "Umarga",
            "stateName": "Maharashtra"
        },
        {
            "_id": "801",
            "cityName": "Pratapgarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "802",
            "cityName": "Lingsugur",
            "stateName": "Karnataka"
        },
        {
            "_id": "803",
            "cityName": "Usilampatti",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "804",
            "cityName": "Palia Kalan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "805",
            "cityName": "Wokha",
            "stateName": "Nagaland"
        },
        {
            "_id": "806",
            "cityName": "Rajpipla",
            "stateName": "Gujarat"
        },
        {
            "_id": "807",
            "cityName": "Vijayapura",
            "stateName": "Karnataka"
        },
        {
            "_id": "808",
            "cityName": "Rawatbhata",
            "stateName": "Rajasthan"
        },
        {
            "_id": "809",
            "cityName": "Sangaria",
            "stateName": "Rajasthan"
        },
        {
            "_id": "810",
            "cityName": "Paithan",
            "stateName": "Maharashtra"
        },
        {
            "_id": "811",
            "cityName": "Rahuri",
            "stateName": "Maharashtra"
        },
        {
            "_id": "812",
            "cityName": "Patti",
            "stateName": "Punjab"
        },
        {
            "_id": "813",
            "cityName": "Za_idpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "814",
            "cityName": "Lalsot",
            "stateName": "Rajasthan"
        },
        {
            "_id": "815",
            "cityName": "Maihar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "816",
            "cityName": "Vedaranyam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "817",
            "cityName": "Nawapur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "818",
            "cityName": "Solan",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "819",
            "cityName": "Vapi",
            "stateName": "Gujarat"
        },
        {
            "_id": "820",
            "cityName": "Sanawad",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "821",
            "cityName": "Warisaliganj",
            "stateName": "Bihar"
        },
        {
            "_id": "822",
            "cityName": "Revelganj",
            "stateName": "Bihar"
        },
        {
            "_id": "823",
            "cityName": "Sabalgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "824",
            "cityName": "Tuljapur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "825",
            "cityName": "Simdega",
            "stateName": "Jharkhand"
        },
        {
            "_id": "826",
            "cityName": "Musabani",
            "stateName": "Jharkhand"
        },
        {
            "_id": "827",
            "cityName": "Kodungallur",
            "stateName": "Kerala"
        },
        {
            "_id": "828",
            "cityName": "Phulabani",
            "stateName": "Odisha"
        },
        {
            "_id": "829",
            "cityName": "Umreth",
            "stateName": "Gujarat"
        },
        {
            "_id": "830",
            "cityName": "Narsipatnam",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "831",
            "cityName": "Nautanwa",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "832",
            "cityName": "Rajgir",
            "stateName": "Bihar"
        },
        {
            "_id": "833",
            "cityName": "Yellandu",
            "stateName": "Telangana"
        },
        {
            "_id": "834",
            "cityName": "Sathyamangalam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "835",
            "cityName": "Pilibanga",
            "stateName": "Rajasthan"
        },
        {
            "_id": "836",
            "cityName": "Morshi",
            "stateName": "Maharashtra"
        },
        {
            "_id": "837",
            "cityName": "Pehowa",
            "stateName": "Haryana"
        },
        {
            "_id": "838",
            "cityName": "Sonepur",
            "stateName": "Bihar"
        },
        {
            "_id": "839",
            "cityName": "Pappinisseri",
            "stateName": "Kerala"
        },
        {
            "_id": "840",
            "cityName": "Zamania",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "841",
            "cityName": "Mihijam",
            "stateName": "Jharkhand"
        },
        {
            "_id": "842",
            "cityName": "Purna",
            "stateName": "Maharashtra"
        },
        {
            "_id": "843",
            "cityName": "Puliyankudi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "844",
            "cityName": "Shikarpur, Bulandshahr",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "845",
            "cityName": "Umaria",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "846",
            "cityName": "Porsa",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "847",
            "cityName": "Naugawan Sadat",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "848",
            "cityName": "Fatehpur Sikri",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "849",
            "cityName": "Manuguru",
            "stateName": "Telangana"
        },
        {
            "_id": "850",
            "cityName": "Udaipur",
            "stateName": "Tripura"
        },
        {
            "_id": "851",
            "cityName": "Pipar City",
            "stateName": "Rajasthan"
        },
        {
            "_id": "852",
            "cityName": "Pattamundai",
            "stateName": "Odisha"
        },
        {
            "_id": "853",
            "cityName": "Nanjikottai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "854",
            "cityName": "Taranagar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "855",
            "cityName": "Yerraguntla",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "856",
            "cityName": "Satana",
            "stateName": "Maharashtra"
        },
        {
            "_id": "857",
            "cityName": "Sherghati",
            "stateName": "Bihar"
        },
        {
            "_id": "858",
            "cityName": "Sankeshwara",
            "stateName": "Karnataka"
        },
        {
            "_id": "859",
            "cityName": "Madikeri",
            "stateName": "Karnataka"
        },
        {
            "_id": "860",
            "cityName": "Thuraiyur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "861",
            "cityName": "Sanand",
            "stateName": "Gujarat"
        },
        {
            "_id": "862",
            "cityName": "Rajula",
            "stateName": "Gujarat"
        },
        {
            "_id": "863",
            "cityName": "Kyathampalle",
            "stateName": "Telangana"
        },
        {
            "_id": "864",
            "cityName": "Shahabad, Rampur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "865",
            "cityName": "Tilda Newra",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "866",
            "cityName": "Narsinghgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "867",
            "cityName": "Chittur-Thathamangalam",
            "stateName": "Kerala"
        },
        {
            "_id": "868",
            "cityName": "Malaj Khand",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "869",
            "cityName": "Sarangpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "870",
            "cityName": "Robertsganj",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "871",
            "cityName": "Sirkali",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "872",
            "cityName": "Radhanpur",
            "stateName": "Gujarat"
        },
        {
            "_id": "873",
            "cityName": "Tiruchendur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "874",
            "cityName": "Utraula",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "875",
            "cityName": "Patratu",
            "stateName": "Jharkhand"
        },
        {
            "_id": "876",
            "cityName": "Vijainagar, Ajmer",
            "stateName": "Rajasthan"
        },
        {
            "_id": "877",
            "cityName": "Periyasemur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "878",
            "cityName": "Pathri",
            "stateName": "Maharashtra"
        },
        {
            "_id": "879",
            "cityName": "Sadabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "880",
            "cityName": "Talikota",
            "stateName": "Karnataka"
        },
        {
            "_id": "881",
            "cityName": "Sinnar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "882",
            "cityName": "Mungeli",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "883",
            "cityName": "Sedam",
            "stateName": "Karnataka"
        },
        {
            "_id": "884",
            "cityName": "Shikaripur",
            "stateName": "Karnataka"
        },
        {
            "_id": "885",
            "cityName": "Sumerpur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "886",
            "cityName": "Sattur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "887",
            "cityName": "Sugauli",
            "stateName": "Bihar"
        },
        {
            "_id": "888",
            "cityName": "Lumding",
            "stateName": "Assam"
        },
        {
            "_id": "889",
            "cityName": "Vandavasi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "890",
            "cityName": "Titlagarh",
            "stateName": "Odisha"
        },
        {
            "_id": "891",
            "cityName": "Uchgaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "892",
            "cityName": "Mokokchung",
            "stateName": "Nagaland"
        },
        {
            "_id": "893",
            "cityName": "Paschim Punropara",
            "stateName": "West Bengal"
        },
        {
            "_id": "894",
            "cityName": "Sagwara",
            "stateName": "Rajasthan"
        },
        {
            "_id": "895",
            "cityName": "Ramganj Mandi",
            "stateName": "Rajasthan"
        },
        {
            "_id": "896",
            "cityName": "Tarakeswar",
            "stateName": "West Bengal"
        },
        {
            "_id": "897",
            "cityName": "Mahalingapura",
            "stateName": "Karnataka"
        },
        {
            "_id": "898",
            "cityName": "Dharmanagar",
            "stateName": "Tripura"
        },
        {
            "_id": "899",
            "cityName": "Mahemdabad",
            "stateName": "Gujarat"
        },
        {
            "_id": "900",
            "cityName": "Manendragarh",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "901",
            "cityName": "Uran",
            "stateName": "Maharashtra"
        },
        {
            "_id": "902",
            "cityName": "Tharamangalam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "903",
            "cityName": "Tirukkoyilur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "904",
            "cityName": "Pen",
            "stateName": "Maharashtra"
        },
        {
            "_id": "905",
            "cityName": "Makhdumpur",
            "stateName": "Bihar"
        },
        {
            "_id": "906",
            "cityName": "Maner",
            "stateName": "Bihar"
        },
        {
            "_id": "907",
            "cityName": "Oddanchatram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "908",
            "cityName": "Palladam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "909",
            "cityName": "Mundi",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "910",
            "cityName": "Nabarangapur",
            "stateName": "Odisha"
        },
        {
            "_id": "911",
            "cityName": "Mudalagi",
            "stateName": "Karnataka"
        },
        {
            "_id": "912",
            "cityName": "Samalkha",
            "stateName": "Haryana"
        },
        {
            "_id": "913",
            "cityName": "Nepanagar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "914",
            "cityName": "Karjat",
            "stateName": "Maharashtra"
        },
        {
            "_id": "915",
            "cityName": "Ranavav",
            "stateName": "Gujarat"
        },
        {
            "_id": "916",
            "cityName": "Pedana",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "917",
            "cityName": "Pinjore",
            "stateName": "Haryana"
        },
        {
            "_id": "918",
            "cityName": "Lakheri",
            "stateName": "Rajasthan"
        },
        {
            "_id": "919",
            "cityName": "Pasan",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "920",
            "cityName": "Puttur",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "921",
            "cityName": "Vadakkuvalliyur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "922",
            "cityName": "Tirukalukundram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "923",
            "cityName": "Mah_idpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "924",
            "cityName": "Mussoorie",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "925",
            "cityName": "Muvattupuzha",
            "stateName": "Kerala"
        },
        {
            "_id": "926",
            "cityName": "Rasra",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "927",
            "cityName": "Udaipurwati",
            "stateName": "Rajasthan"
        },
        {
            "_id": "928",
            "cityName": "Manwath",
            "stateName": "Maharashtra"
        },
        {
            "_id": "929",
            "cityName": "Adoor",
            "stateName": "Kerala"
        },
        {
            "_id": "930",
            "cityName": "Uthamapalayam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "931",
            "cityName": "Partur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "932",
            "cityName": "Nahan",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "933",
            "cityName": "Ladwa",
            "stateName": "Haryana"
        },
        {
            "_id": "934",
            "cityName": "Mankachar",
            "stateName": "Assam"
        },
        {
            "_id": "935",
            "cityName": "Nongstoin",
            "stateName": "Meghalaya"
        },
        {
            "_id": "936",
            "cityName": "Losal",
            "stateName": "Rajasthan"
        },
        {
            "_id": "937",
            "cityName": "Sri Madhopur",
            "stateName": "Rajasthan"
        },
        {
            "_id": "938",
            "cityName": "Ramngarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "939",
            "cityName": "Mavelikkara",
            "stateName": "Kerala"
        },
        {
            "_id": "940",
            "cityName": "Rawatsar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "941",
            "cityName": "Rajakhera",
            "stateName": "Rajasthan"
        },
        {
            "_id": "942",
            "cityName": "Lar",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "943",
            "cityName": "Lal Gopalganj Nindaura",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "944",
            "cityName": "Muddebihal",
            "stateName": "Karnataka"
        },
        {
            "_id": "945",
            "cityName": "Sirsaganj",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "946",
            "cityName": "Shahpura",
            "stateName": "Rajasthan"
        },
        {
            "_id": "947",
            "cityName": "Surandai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "948",
            "cityName": "Sangole",
            "stateName": "Maharashtra"
        },
        {
            "_id": "949",
            "cityName": "Pavagada",
            "stateName": "Karnataka"
        },
        {
            "_id": "950",
            "cityName": "Tharad",
            "stateName": "Gujarat"
        },
        {
            "_id": "951",
            "cityName": "Mansa",
            "stateName": "Gujarat"
        },
        {
            "_id": "952",
            "cityName": "Umbergaon",
            "stateName": "Gujarat"
        },
        {
            "_id": "953",
            "cityName": "Mavoor",
            "stateName": "Kerala"
        },
        {
            "_id": "954",
            "cityName": "Nalbari",
            "stateName": "Assam"
        },
        {
            "_id": "955",
            "cityName": "Talaja",
            "stateName": "Gujarat"
        },
        {
            "_id": "956",
            "cityName": "Malur",
            "stateName": "Karnataka"
        },
        {
            "_id": "957",
            "cityName": "Mangrulpir",
            "stateName": "Maharashtra"
        },
        {
            "_id": "958",
            "cityName": "Soro",
            "stateName": "Odisha"
        },
        {
            "_id": "959",
            "cityName": "Shahpura",
            "stateName": "Rajasthan"
        },
        {
            "_id": "960",
            "cityName": "Vadnagar",
            "stateName": "Gujarat"
        },
        {
            "_id": "961",
            "cityName": "Raisinghnagar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "962",
            "cityName": "Sindhagi",
            "stateName": "Karnataka"
        },
        {
            "_id": "963",
            "cityName": "Sanduru",
            "stateName": "Karnataka"
        },
        {
            "_id": "964",
            "cityName": "Sohna",
            "stateName": "Haryana"
        },
        {
            "_id": "965",
            "cityName": "Manavadar",
            "stateName": "Gujarat"
        },
        {
            "_id": "966",
            "cityName": "Pihani",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "967",
            "cityName": "Saf_idon",
            "stateName": "Haryana"
        },
        {
            "_id": "968",
            "cityName": "Risod",
            "stateName": "Maharashtra"
        },
        {
            "_id": "969",
            "cityName": "Rosera",
            "stateName": "Bihar"
        },
        {
            "_id": "970",
            "cityName": "Sankari",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "971",
            "cityName": "Malpura",
            "stateName": "Rajasthan"
        },
        {
            "_id": "972",
            "cityName": "Sonamukhi",
            "stateName": "West Bengal"
        },
        {
            "_id": "973",
            "cityName": "Shamsabad, Agra",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "974",
            "cityName": "Nokha",
            "stateName": "Bihar"
        },
        {
            "_id": "975",
            "cityName": "PandUrban Agglomeration",
            "stateName": "West Bengal"
        },
        {
            "_id": "976",
            "cityName": "Mainaguri",
            "stateName": "West Bengal"
        },
        {
            "_id": "977",
            "cityName": "Afzalpur",
            "stateName": "Karnataka"
        },
        {
            "_id": "978",
            "cityName": "Shirur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "979",
            "cityName": "Salaya",
            "stateName": "Gujarat"
        },
        {
            "_id": "980",
            "cityName": "Shenkottai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "981",
            "cityName": "Pratapgarh",
            "stateName": "Tripura"
        },
        {
            "_id": "982",
            "cityName": "Vadipatti",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "983",
            "cityName": "Nagarkurnool",
            "stateName": "Telangana"
        },
        {
            "_id": "984",
            "cityName": "Savner",
            "stateName": "Maharashtra"
        },
        {
            "_id": "985",
            "cityName": "Sasvad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "986",
            "cityName": "Rudrapur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "987",
            "cityName": "Soron",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "988",
            "cityName": "Sholingur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "989",
            "cityName": "Pandharkaoda",
            "stateName": "Maharashtra"
        },
        {
            "_id": "990",
            "cityName": "Perumbavoor",
            "stateName": "Kerala"
        },
        {
            "_id": "991",
            "cityName": "Maddur",
            "stateName": "Karnataka"
        },
        {
            "_id": "992",
            "cityName": "Nadbai",
            "stateName": "Rajasthan"
        },
        {
            "_id": "993",
            "cityName": "Talode",
            "stateName": "Maharashtra"
        },
        {
            "_id": "994",
            "cityName": "Shrigonda",
            "stateName": "Maharashtra"
        },
        {
            "_id": "995",
            "cityName": "Madhugiri",
            "stateName": "Karnataka"
        },
        {
            "_id": "996",
            "cityName": "Tekkalakote",
            "stateName": "Karnataka"
        },
        {
            "_id": "997",
            "cityName": "Seoni-Malwa",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "998",
            "cityName": "Shirdi",
            "stateName": "Maharashtra"
        },
        {
            "_id": "999",
            "cityName": "SUrban Agglomerationr",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1000",
            "cityName": "Terdal",
            "stateName": "Karnataka"
        },
        {
            "_id": "1001",
            "cityName": "Raver",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1002",
            "cityName": "Tirupathur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1003",
            "cityName": "Taraori",
            "stateName": "Haryana"
        },
        {
            "_id": "1004",
            "cityName": "Mukhed",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1005",
            "cityName": "Manachanallur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1006",
            "cityName": "Rehli",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1007",
            "cityName": "Sanchore",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1008",
            "cityName": "Rajura",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1009",
            "cityName": "Piro",
            "stateName": "Bihar"
        },
        {
            "_id": "1010",
            "cityName": "Mudab_idri",
            "stateName": "Karnataka"
        },
        {
            "_id": "1011",
            "cityName": "Vadgaon Kasba",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1012",
            "cityName": "Nagar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1013",
            "cityName": "Vijapur",
            "stateName": "Gujarat"
        },
        {
            "_id": "1014",
            "cityName": "Viswanatham",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1015",
            "cityName": "Polur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1016",
            "cityName": "Panagudi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1017",
            "cityName": "Manawar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1018",
            "cityName": "Tehri",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "1019",
            "cityName": "Samdhan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1020",
            "cityName": "Pardi",
            "stateName": "Gujarat"
        },
        {
            "_id": "1021",
            "cityName": "Rahatgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1022",
            "cityName": "Panagar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1023",
            "cityName": "Uthiramerur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1024",
            "cityName": "Tirora",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1025",
            "cityName": "Rangia",
            "stateName": "Assam"
        },
        {
            "_id": "1026",
            "cityName": "Sahjanwa",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1027",
            "cityName": "Wara Seoni",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1028",
            "cityName": "Magadi",
            "stateName": "Karnataka"
        },
        {
            "_id": "1029",
            "cityName": "Rajgarh (Alwar)",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1030",
            "cityName": "Rafiganj",
            "stateName": "Bihar"
        },
        {
            "_id": "1031",
            "cityName": "Tarana",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1032",
            "cityName": "Rampur Maniharan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1033",
            "cityName": "Sheoganj",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1034",
            "cityName": "Raikot",
            "stateName": "Punjab"
        },
        {
            "_id": "1035",
            "cityName": "Pauri",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "1036",
            "cityName": "Sumerpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1037",
            "cityName": "Navalgund",
            "stateName": "Karnataka"
        },
        {
            "_id": "1038",
            "cityName": "Shahganj",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1039",
            "cityName": "Marhaura",
            "stateName": "Bihar"
        },
        {
            "_id": "1040",
            "cityName": "Tulsipur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1041",
            "cityName": "Sadri",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1042",
            "cityName": "Thiruthuraipoondi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1043",
            "cityName": "Shiggaon",
            "stateName": "Karnataka"
        },
        {
            "_id": "1044",
            "cityName": "Pallapatti",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1045",
            "cityName": "Mahendragarh",
            "stateName": "Haryana"
        },
        {
            "_id": "1046",
            "cityName": "Sausar",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1047",
            "cityName": "Ponneri",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1048",
            "cityName": "Mahad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1049",
            "cityName": "Lohardaga",
            "stateName": "Jharkhand"
        },
        {
            "_id": "1050",
            "cityName": "Tirwaganj",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1051",
            "cityName": "Margherita",
            "stateName": "Assam"
        },
        {
            "_id": "1052",
            "cityName": "Sundarnagar",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "1053",
            "cityName": "Rajgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1054",
            "cityName": "Mangaldoi",
            "stateName": "Assam"
        },
        {
            "_id": "1055",
            "cityName": "Renigunta",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "1056",
            "cityName": "Longowal",
            "stateName": "Punjab"
        },
        {
            "_id": "1057",
            "cityName": "Ratia",
            "stateName": "Haryana"
        },
        {
            "_id": "1058",
            "cityName": "Lalgudi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1059",
            "cityName": "Shrirangapattana",
            "stateName": "Karnataka"
        },
        {
            "_id": "1060",
            "cityName": "Niwari",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1061",
            "cityName": "Natham",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1062",
            "cityName": "Unnamalaikadai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1063",
            "cityName": "PurqUrban Agglomerationzi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1064",
            "cityName": "Shamsabad, Farrukhabad",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1065",
            "cityName": "Mirganj",
            "stateName": "Bihar"
        },
        {
            "_id": "1066",
            "cityName": "Todaraisingh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1067",
            "cityName": "Warhapur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1068",
            "cityName": "Rajam",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "1069",
            "cityName": "Urmar Tanda",
            "stateName": "Punjab"
        },
        {
            "_id": "1070",
            "cityName": "Lonar",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1071",
            "cityName": "Powayan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1072",
            "cityName": "P.N.Patti",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1073",
            "cityName": "Palampur",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "1074",
            "cityName": "Srisailam Project (Right Flank Colony) Township",
            "stateName": "Andhra Pradesh"
        },
        {
            "_id": "1075",
            "cityName": "Sindagi",
            "stateName": "Karnataka"
        },
        {
            "_id": "1076",
            "cityName": "Sandi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1077",
            "cityName": "Vaikom",
            "stateName": "Kerala"
        },
        {
            "_id": "1078",
            "cityName": "Malda",
            "stateName": "West Bengal"
        },
        {
            "_id": "1079",
            "cityName": "Tharangambadi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1080",
            "cityName": "Sakaleshapura",
            "stateName": "Karnataka"
        },
        {
            "_id": "1081",
            "cityName": "Lalganj",
            "stateName": "Bihar"
        },
        {
            "_id": "1082",
            "cityName": "Malkangiri",
            "stateName": "Odisha"
        },
        {
            "_id": "1083",
            "cityName": "Rapar",
            "stateName": "Gujarat"
        },
        {
            "_id": "1084",
            "cityName": "Mauganj",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1085",
            "cityName": "Todabhim",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1086",
            "cityName": "Srinivaspur",
            "stateName": "Karnataka"
        },
        {
            "_id": "1087",
            "cityName": "Murliganj",
            "stateName": "Bihar"
        },
        {
            "_id": "1088",
            "cityName": "Reengus",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1089",
            "cityName": "Sawantwadi",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1090",
            "cityName": "Tittakudi",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1091",
            "cityName": "Lilong",
            "stateName": "Manipur"
        },
        {
            "_id": "1092",
            "cityName": "Rajaldesar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1093",
            "cityName": "Pathardi",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1094",
            "cityName": "Achhnera",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1095",
            "cityName": "Pacode",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1096",
            "cityName": "Naraura",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1097",
            "cityName": "Nakur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1098",
            "cityName": "Palai",
            "stateName": "Kerala"
        },
        {
            "_id": "1099",
            "cityName": "Morinda, India",
            "stateName": "Punjab"
        },
        {
            "_id": "1100",
            "cityName": "Manasa",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1101",
            "cityName": "Nainpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1102",
            "cityName": "Sahaspur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1103",
            "cityName": "Pauni",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1104",
            "cityName": "Prithvipur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1105",
            "cityName": "Ramtek",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1106",
            "cityName": "Silapathar",
            "stateName": "Assam"
        },
        {
            "_id": "1107",
            "cityName": "Songadh",
            "stateName": "Gujarat"
        },
        {
            "_id": "1108",
            "cityName": "Safipur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1109",
            "cityName": "Sohagpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1110",
            "cityName": "Mul",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1111",
            "cityName": "Sadulshahar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1112",
            "cityName": "Phillaur",
            "stateName": "Punjab"
        },
        {
            "_id": "1113",
            "cityName": "Sambhar",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1114",
            "cityName": "Prantij",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1115",
            "cityName": "Nagla",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "1116",
            "cityName": "Pattran",
            "stateName": "Punjab"
        },
        {
            "_id": "1117",
            "cityName": "Mount Abu",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1118",
            "cityName": "Reoti",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1119",
            "cityName": "Tenu dam-cum-Kathhara",
            "stateName": "Jharkhand"
        },
        {
            "_id": "1120",
            "cityName": "Panchla",
            "stateName": "West Bengal"
        },
        {
            "_id": "1121",
            "cityName": "Sitarganj",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "1122",
            "cityName": "Pasighat",
            "stateName": "Arunachal Pradesh"
        },
        {
            "_id": "1123",
            "cityName": "Motipur",
            "stateName": "Bihar"
        },
        {
            "_id": "1124",
            "cityName": "O' Valley",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1125",
            "cityName": "Raghunathpur",
            "stateName": "West Bengal"
        },
        {
            "_id": "1126",
            "cityName": "Suriyampalayam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1127",
            "cityName": "Qadian",
            "stateName": "Punjab"
        },
        {
            "_id": "1128",
            "cityName": "Rairangpur",
            "stateName": "Odisha"
        },
        {
            "_id": "1129",
            "cityName": "Silvassa",
            "stateName": "Dadra and Nagar Haveli"
        },
        {
            "_id": "1130",
            "cityName": "Nowrozabad (Khodargama)",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1131",
            "cityName": "Mangrol",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1132",
            "cityName": "Soyagaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1133",
            "cityName": "Sujanpur",
            "stateName": "Punjab"
        },
        {
            "_id": "1134",
            "cityName": "Manihari",
            "stateName": "Bihar"
        },
        {
            "_id": "1135",
            "cityName": "Sikanderpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1136",
            "cityName": "Mangalvedhe",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1137",
            "cityName": "Phulera",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1138",
            "cityName": "Ron",
            "stateName": "Karnataka"
        },
        {
            "_id": "1139",
            "cityName": "Sholavandan",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1140",
            "cityName": "Sa_idpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1141",
            "cityName": "Shamgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1142",
            "cityName": "Thammampatti",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1143",
            "cityName": "Maharajpur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1144",
            "cityName": "Multai",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1145",
            "cityName": "Mukerian",
            "stateName": "Punjab"
        },
        {
            "_id": "1146",
            "cityName": "Sirsi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1147",
            "cityName": "Purwa",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1148",
            "cityName": "Sheohar",
            "stateName": "Bihar"
        },
        {
            "_id": "1149",
            "cityName": "Namagiripettai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1150",
            "cityName": "Parasi",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1151",
            "cityName": "Lathi",
            "stateName": "Gujarat"
        },
        {
            "_id": "1152",
            "cityName": "Lalganj",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1153",
            "cityName": "Narkhed",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1154",
            "cityName": "Mathabhanga",
            "stateName": "West Bengal"
        },
        {
            "_id": "1155",
            "cityName": "Shendurjana",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1156",
            "cityName": "Peravurani",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1157",
            "cityName": "Mariani",
            "stateName": "Assam"
        },
        {
            "_id": "1158",
            "cityName": "Phulpur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1159",
            "cityName": "Rania",
            "stateName": "Haryana"
        },
        {
            "_id": "1160",
            "cityName": "Pali",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1161",
            "cityName": "Pachore",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1162",
            "cityName": "Parangipettai",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1163",
            "cityName": "Pudupattinam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1164",
            "cityName": "Panniyannur",
            "stateName": "Kerala"
        },
        {
            "_id": "1165",
            "cityName": "Maharajganj",
            "stateName": "Bihar"
        },
        {
            "_id": "1166",
            "cityName": "Rau",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1167",
            "cityName": "Monoharpur",
            "stateName": "West Bengal"
        },
        {
            "_id": "1168",
            "cityName": "Mandawa",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1169",
            "cityName": "Marigaon",
            "stateName": "Assam"
        },
        {
            "_id": "1170",
            "cityName": "Pallikonda",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1171",
            "cityName": "Pindwara",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1172",
            "cityName": "Shishgarh",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1173",
            "cityName": "Patur",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1174",
            "cityName": "Mayang Imphal",
            "stateName": "Manipur"
        },
        {
            "_id": "1175",
            "cityName": "Mhowgaon",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1176",
            "cityName": "Guruvayoor",
            "stateName": "Kerala"
        },
        {
            "_id": "1177",
            "cityName": "Mhaswad",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1178",
            "cityName": "Sahawar",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1179",
            "cityName": "Sivagiri",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1180",
            "cityName": "Mundargi",
            "stateName": "Karnataka"
        },
        {
            "_id": "1181",
            "cityName": "Punjaipugalur",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1182",
            "cityName": "Kailasahar",
            "stateName": "Tripura"
        },
        {
            "_id": "1183",
            "cityName": "Samthar",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1184",
            "cityName": "Sakti",
            "stateName": "Chhattisgarh"
        },
        {
            "_id": "1185",
            "cityName": "Sadalagi",
            "stateName": "Karnataka"
        },
        {
            "_id": "1186",
            "cityName": "Silao",
            "stateName": "Bihar"
        },
        {
            "_id": "1187",
            "cityName": "Mandalgarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1188",
            "cityName": "Loha",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1189",
            "cityName": "Pukhrayan",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1190",
            "cityName": "Padmanabhapuram",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1191",
            "cityName": "Belonia",
            "stateName": "Tripura"
        },
        {
            "_id": "1192",
            "cityName": "Saiha",
            "stateName": "Mizoram"
        },
        {
            "_id": "1193",
            "cityName": "Srirampore",
            "stateName": "West Bengal"
        },
        {
            "_id": "1194",
            "cityName": "Talwara",
            "stateName": "Punjab"
        },
        {
            "_id": "1195",
            "cityName": "Puthuppally",
            "stateName": "Kerala"
        },
        {
            "_id": "1196",
            "cityName": "Khowai",
            "stateName": "Tripura"
        },
        {
            "_id": "1197",
            "cityName": "Vijaypur",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1198",
            "cityName": "Takhatgarh",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1199",
            "cityName": "Thirupuvanam",
            "stateName": "Tamil Nadu"
        },
        {
            "_id": "1200",
            "cityName": "Adra",
            "stateName": "West Bengal"
        },
        {
            "_id": "1201",
            "cityName": "Piriyapatna",
            "stateName": "Karnataka"
        },
        {
            "_id": "1202",
            "cityName": "Obra",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1203",
            "cityName": "Adalaj",
            "stateName": "Gujarat"
        },
        {
            "_id": "1204",
            "cityName": "Nandgaon",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1205",
            "cityName": "Barh",
            "stateName": "Bihar"
        },
        {
            "_id": "1206",
            "cityName": "Chhapra",
            "stateName": "Gujarat"
        },
        {
            "_id": "1207",
            "cityName": "Panamattom",
            "stateName": "Kerala"
        },
        {
            "_id": "1208",
            "cityName": "Niwai",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1209",
            "cityName": "Bageshwar",
            "stateName": "Uttarakhand"
        },
        {
            "_id": "1210",
            "cityName": "Tarbha",
            "stateName": "Odisha"
        },
        {
            "_id": "1211",
            "cityName": "Adyar",
            "stateName": "Karnataka"
        },
        {
            "_id": "1212",
            "cityName": "Narsinghgarh",
            "stateName": "Madhya Pradesh"
        },
        {
            "_id": "1213",
            "cityName": "Warud",
            "stateName": "Maharashtra"
        },
        {
            "_id": "1214",
            "cityName": "Asarganj",
            "stateName": "Bihar"
        },
        {
            "_id": "1215",
            "cityName": "Sarsod",
            "stateName": "Haryana"
        },
        {
            "_id": "1216",
            "cityName": "Gandhinagar",
            "stateName": "Gujarat"
        },
        {
            "_id": "1217",
            "cityName": "Kullu",
            "stateName": "Himachal Pradesh"
        },
        {
            "_id": "1218",
            "cityName": "Manali",
            "stateName": "Himachal Praddesh"
        },
        {
            "_id": "1219",
            "cityName": "Mirzapur",
            "stateName": "Uttar Pradesh"
        },
        {
            "_id": "1220",
            "cityName": "Kota",
            "stateName": "Rajasthan"
        },
        {
            "_id": "1221",
            "cityName": "Dispur",
            "stateName": "Assam"
        }
    ];

    await city.create(cityObj);

}

module.exports.getCityList = async (req, res, next) => {

    let stateName = req.query.stateName;
    const cityList = await city.find({ stateName: stateName }).exec();
    if (cityList.length) {
        return res.json({
            success: true,
            result: cityList,
            message: "Successfully fetched.."
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No data"
        })
    }
}

module.exports.addCity = async (req, res, next) => {

    let { id, customerCare, phone, uploadPhoto } = req.body;

    const updateData = await city.findOneAndUpdate(
        { _id: id },
        {
            $set: {
                customerCare: customerCare,
                phone: phone,
                uploadPhoto: uploadPhoto,
                isActive: true
            }
        },
        { new: true }
    );

    if (updateData) {
        return res.json({
            success: true,
            message: "Successfully updated state.."
        })
    }
    else {
        return res.json({
            success: false,
            message: "Error occured"
        })
    }

}

module.exports.getPgByCity = async(req,res,next)=>{
    const pgDetail = await pgDetails.find({city:req.query.cityName})
    if(pgDetail){
        return res.json({
            success:true,
            result:pgDetail,
        message:"pg's are fetched"})
    }else{
        return res.json({
            success:false,
            message:"No pg found"
        })
    }
}