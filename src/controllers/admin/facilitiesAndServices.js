const facilitiesSchema = require("../../models/admin/facilities");
const activeServiceSchema = require("../../models/admin/activeServices")
const roomFacilities = require("../../models/admin/roomFacilities")
const complaintsSchema = require("../../models/admin/complaints")
const complaintsFeatureSchema = require("../../models/admin/complaintsFeature")
const offerSchema = require("../../models/admin/offerMaster")

// @desc create facilities
// @route post api/v1/createFacilities
// access public
module.exports.createFacilities = async (req, res, next) => {
    let getSchema = await facilitiesSchema.find();
    if (getSchema.length) {
        const updateFacilities = await facilitiesSchema.updateOne(
            { _id: getSchema[0]._id },
            {
                $push: {
                    "facilities": {
                        name: req.body.name,
                        imageUrl: req.body.imageUrl,
                    }
                }
            }, { new: true }
        )
        console.log(updateFacilities)
        if (updateFacilities) {
            return res.json({
                success: true,
                message: "Facilities Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Facilities Not Added",
            });
        }
    }
    else {
        var addFacilities = new facilitiesSchema({
            facilities: [
                {
                    name: req.body.name,
                    imageUrl: req.body.imageUrl,
                },
            ]
        })
        const saveData = await addFacilities.save();
        if (saveData) {
            return res.json({
                success: true,
                message: "Facilities Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Facilities Not Added",
            });
        }
    }
}


// @desc get all facilities
// @route get api/v1/getFacilities
// access public
module.exports.getFacilities = async (req, res, next) => {
    let getFacilities = await facilitiesSchema.find();
    if (getFacilities.length) {
        return res.json({
            success: true,
            data: getFacilities[0].facilities,
            message: "Get Facilities SuccessFully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "Facilities is Empty",
        });
    }
}


// @desc create active services
// @route post api/v1/createActiveService
// access public
module.exports.createActiveService = async (req, res, next) => {
    let getSchema = await activeServiceSchema.find();
    if (getSchema.length) {
        const updateService = await activeServiceSchema.updateOne(
            { _id: getSchema[0]._id },
            {
                $push: {
                    "services": {
                        name: req.body.name,
                        imageUrl: req.body.imageUrl,
                    }
                }
            }, { new: true }
        ).exec();
        if (updateService) {
            return res.json({
                success: true,
                message: "services Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "services Not Added",
            });
        }
    }
    else {
        var addService = new activeServiceSchema({
            services: [
                {
                    name: req.body.name,
                    imageUrl: req.body.imageUrl,
                },
            ]
        })
        const saveData = await addService.save();
        if (saveData) {
            return res.json({
                success: true,
                message: "services Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "services Not Added",
            });
        }
    }
}
// @desc get all the active services
// @route get api/v1/getActiveService
// access public
module.exports.getActiveService = async (req, res, next) => {
    let getServices = await activeServiceSchema.find();
    if (getServices.length) {
        return res.json({
            success: true,
            data: getServices[0].services,
            message: "Get Services SuccessFully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "services is Empty",
        });
    }
}


// @desc create Room facilities
// @route post api/v1/createRoomFacilities
// access public
module.exports.createRoomFacilities = async (req, res, next) => {
    let getSchema = await roomFacilities.find();
    if (getSchema.length) {
        const updateRoomFacilities = await roomFacilities.findOneAndUpdate(
            { _id: getSchema[0]._id },
            {
                $push: {
                    "facilities": {
                        name: req.body.name,
                        imageUrl: req.body.imageUrl,
                    }
                }
            }, { new: true }
        ).exec();
        if (updateRoomFacilities) {
            return res.json({
                success: true,
                message: "Facilities Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Facilities Not Added",
            });
        }
    }
    else {
        var addRoomFacilities = new roomFacilities({
            facilities: [
                {
                    name: req.body.name,
                    imageUrl: req.body.imageUrl,
                },
            ]
        })
        const saveData = await addRoomFacilities.save();
        if (saveData) {
            return res.json({
                success: true,
                message: "Facilities Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Facilities Not Added",
            });
        }
    }
}

// @desc get all Room facilities
// @route get api/v1/getRoomFacilities
// access public
module.exports.getRoomFacilities = async (req, res, next) => {
    let getRoomFacilities = await roomFacilities.find();
    if (getRoomFacilities.length) {
        return res.json({
            success: true,
            data: getRoomFacilities[0].facilities,
            message: "Get Facilities SuccessFully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "Facilities is Empty",
        });
    }

}

// @desc create New Complaintes
// @route post api/v1/createComplaints
// access public
module.exports.createComplaints = async (req, res, next) => {
    const complaintInfo = await complaintsSchema.findOne({ name: req.body.name }).exec();
    console.log(complaintInfo)
    if (!complaintInfo) {
        const data = new complaintsSchema({
            name: req.body.name,
            description: req.body.description,
            imageUrl: req.body.imageUrl
        });
        const saveData = await data.save();
        if (saveData) {
            return res.json({
                success: true,
                result: data,
                id: data._id,
                message: "Created Successfully!"
            });
        }
        else {
            return res.json({
                success: false,
                message: "Error occured!"
            });
        }
    }
    else {
        return res.json({
            success: false,
            message: "Name is already Available"
        });
    }
}


// @desc create facilities
// @route post api/v1/createComplaintsFeature
// access public
module.exports.createComplaintsFeature = async (req, res, next) => {
    if (req.body.type == 0) {
        var addCompFeature = new complaintsFeatureSchema({
            comp_id: req.body.comp_id,
            compName: req.body.compName,
            name: req.body.name,
            image: req.body.image,
            items: req.body.items,
        })
        const saveData = await addCompFeature.save();
        if (saveData) {
            return res.json({
                success: true,
                result: saveData,
                id: saveData._id,
                message: "Feature Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Failed Not Added",
            });
        }
    }
    else if (req.body.type == 1) {
        const updateComp = await complaintsFeatureSchema.findOneAndUpdate(
            { _id: req.body.featureId },
            {
                $push: {
                    "items": req.body.items
                }
            }, { new: true }
        ).exec();
        if (updateComp) {
            return res.json({
                success: true,
                result: updateComp,
                id: updateComp._id,
                message: "Feature Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Failed Not Added",
            });
        }
    }
    else {
        return res.json({
            success: false,
            message: "Type is not available",
        });
    }
}

// @desc get complaints master
// @route post api/v1/getComplaints
// access public
module.exports.getComplaints = async (req, res, next) => {

    let complaintsData = await complaintsSchema.find();
    if (complaintsData.length) {
        return res.json({
            success: true,
            result: complaintsData,
            message: "Successfully fetched"
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No complaints"
        })
    }
}

// @desc get complaints master
// @route post api/v1/getComplaintsById
// access public
module.exports.getComplaintsById = async (req, res, next) => {
    const complaintData = await complaintsFeatureSchema.find({ comp_id: req.query.complaintId })
    if (complaintData.length) {
        return res.json({
            success: true,
            result: complaintData,
            message: "comaplints are fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Please Add Data"
        })
    }
}

// @desc create New offer
// @route post api/v1/createOffer
// access public
module.exports.createOffer = async (req, res, next) => {
    const data = new offerSchema({
        name: req.body.name,
        percentage: req.body.percentage,
        code: req.body.code,
        expireDate: req.body.expireDate,
        image: req.body.image
    });
    const saveData = await data.save();
    if (saveData) {
        return res.json({
            success: true,
            result: data,
            id: data._id,
            message: "Created Successfully!"
        });
    }
    else {
        return res.json({
            success: false,
            message: "Error occured!"
        });
    }
}

// @desc get All offer
// @route post api/v1/getOffer
// access public
module.exports.getAllOffer = async (req, res, next) => {
    let getSchema = await offerSchema.find();
    if (getSchema.length) {
        return res.json({
            success: true,
            result: getSchema,
            message: "Offers Fetched Successfully!"
        });
    }
    else {
        return res.json({
            success: false,
            message: "Error occured!"
        });
    }
}