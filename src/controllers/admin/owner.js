const pgSchema = require("../../models/admin/pgSchema");
const roomSchema = require("../../models/admin/roomDetails")
const activeServicePgData = require("../../models/admin/activeServicePgData");
const menuSchema = require("../../models/admin/foodMenuList")
const bedSchema = require("../../models/admin/bedDetails")
const stateSchema = require("../../models/admin/state")
const citySchema = require("../../models/admin/city")
const addOfferSchema = require("../../models/admin/offerPg")
const { isFunction } = require("util");

// @desc add owner
// @route post api/v1/addPgDetails
// access public
module.exports.addPgDetails = async (req, res, next) => {
    var createNewPg = new pgSchema({
        pgOwnerName: req.body.pgOwnerName,
        pgNumber: req.body.pgNumber,
        pgOwnerEmail: req.body.pgOwnerEmail,
        pgOwnerNumber: req.body.pgOwnerNumber,
        pgName: req.body.pgName,
        forGender: req.body.gender,
        state: req.body.state,
        city: req.body.city,
        landmark: req.body.landmark,
        addressLine1: req.body.addressLine1,
        addressLine2: req.body.addressLine2,
        facilities: req.body.facilities,
        thumbnail: req.body.thumbnail,
        pgImage: req.body.pgImage,
        isActive: true,
        created_at: Date.now()
    })
    const saveAllData = await createNewPg.save();
    if (saveAllData) {
        return res.json({
            success: true,
            pgId: saveAllData._id,
            message: "Pg Registered Successfully"
        })
    }
    else {
        return res.json({
            success: false,
            message: "Error in save data"
        })
    }
}

// @desc add Service
// @route post api/v1/addServices
// access public
module.exports.addServices = async (req, res, next) => {
    let getSchema = await activeServicePgData.find({ pg_id: req.body.pgId });
    if (getSchema.length) {
        const updateService = await activeServicePgData.updateOne({ _id: getSchema._id },
            {
                $push: {
                    "services": req.body.services
                }
            }, { new: true }
        ).exec();
        if (updateService) {
            return res.json({
                success: true,
                message: "services Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "services Not Added",
            });
        }
    }
    else {
        var addService = new activeServicePgData({
            pg_id: req.body.pgId,
            services: req.body.services
        })
        const saveData = await addService.save();
        if (saveData) {
            return res.json({
                success: true,
                message: "services Added SuccessFully",
            });
        }
        else {
            return res.json({
                success: false,
                message: "services Not Added",
            });
        }
    }
}

// @desc add Menu List
// @route post api/v1/addMenuList
// access public
module.exports.addMenuList = async (req, res, next) => {

    var addmenuList = new menuSchema({
        pg_id: req.body.pgId,
        breakfast: req.body.breakfast,
        lunch: req.body.lunch,
        dinner: req.body.dinner
    })

    const saveData = addmenuList.save()
    if (saveData) {
        return res.json({
            success: true,
            message: "Menu Added SuccessFully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "Menu Not Added",
        });
    }

}

// @desc add Room and Bed details
// @route post api/v1/addRoomDetails
// access public
module.exports.addRoomDetails = async (req, res, next) => {
    let pgId = req.body.pgId;
    let beds = req.body.bedDetails

    if (beds.length) {
        const saveRoomDetails = new roomSchema({
            pg_id: pgId,
            roomNumber: req.body.roomNumber,
            sharing: req.body.numberOfBeds,
            basicRentPerMonth: req.body.basicRentPerMonth,
            roomDeposit: req.body.roomDeposit,
            floorNo: req.body.floorNo,
            roomType: req.body.roomType,
            facilities: req.body.facilities,
            roomImage: req.body.roomImage,
            created_at: Date.now()
        })
        const saveRoom = await saveRoomDetails.save();

        if (saveRoom) {
            beds.forEach((singleBed, i) => {
                console.log(singleBed)
                if (i + 1 == beds.length) {
                    createNewbed(singleBed, saveRoom._id, pgId, true, res)
                }
                else {
                    createNewbed(singleBed, saveRoom._id, pgId, false, res)
                }
            })
        }
        else {
            return res.json({
                success: false,
                message: "Not Able to Save Room"
            })
        }
    }
    else {
        return res.json({
            success: false,
            message: "Bed is Empty"
        })
    }

}

// @desc Submit Room Main form
// @route post api/v1/submitRoom
// access public
module.exports.submitRoom = async (req, res, next) => {

    let totalRoomsFloor = req.body.noOfRoomsPerFloor;
    let arrFloor = []
    let totalRoom = 0;
    if (totalRoomsFloor.length) {
        totalRoomsFloor.forEach((data, i) => {
            var floorRoomDetails = {
                floorName: i,
                totalRoom: data
            };
            arrFloor.push(floorRoomDetails);
            totalRoom = totalRoom + data;
        })
        let getAllBed = await bedSchema.find({ pg_id: req.body.pgId });
        const updateRoomdata = await pgSchema.updateOne(
            { _id: req.body.pgId },
            {
                $push: {
                    "noOfRoomsPerFloor": arrFloor
                },
                noOfFloor: totalRoomsFloor.length,
                totalNumberOfRoom: totalRoom,
                totalNumberOfBeds: getAllBed.length
            }, { new: true }
        ).exec();
        if (updateRoomdata) {
            return res.json({
                success: true,
                message: "Saved SuccessFully"
            });
        } else {
            return res.json({
                success: false,
                message: "Data Not Saved"
            });
        }
    }
    else {
        return res.json({
            success: false,
            message: "Data Not Saved"
        });
    }

}

// @desc single pg All data 
// @route get api/v1/getAllDataPg
// access public
module.exports.getAllDataPg = async (req, res, next) => {
    let getPG = await pgSchema.findOne({ _id: req.query.pgId, isActive: true });
    if (getPG) {
        let getPGRooms = await roomSchema.find({ pg_id: req.query.pgId });
        if (getPGRooms.length) {
            return res.json({
                success: true,
                pgDetails: getPG,
                roomDetails: getPGRooms,
                message: "Saved SuccessFully"
            });
        }
        else {
            return res.json({
                success: true,
                pgDetails: getPG,
                message: "Roon's Not available or Not Added"
            });
        }

    }
    else {
        return res.json({
            success: false,
            message: "No Data Found"
        });
    }
}

// @desc get All the Pg
// @route get api/v1/getAllPg
// access public
module.exports.getAllPg = async (req, res, next) => {
    let allPgList = await pgSchema.find({ isActive: true });
    if (allPgList.length) {
        return res.json({
            success: true,
            result: allPgList,
            message: "fetched Succeessfully"
        });
    }
    else {
        return res.json({
            success: false,
            message: "Data Not Saved"
        });
    }
}
// @desc get Add State
// @route get api/v1/getAddState
// access public
module.exports.getAddState = async (req, res, next) => {

    const stateList = await stateSchema.distinct("stateName", { isActive: true }).exec();
    if (stateList.length) {
        return res.json({
            success: true,
            result: stateList,
            message: "Successfully fetched.."
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No data"
        })
    }
}

// @desc get Add City
// @route get api/v1/getAddCity
// access public
module.exports.getAddCity = async (req, res, next) => {
    let state_Name = req.query.stateName
    const stateList = await citySchema.distinct("cityName", { stateName: state_Name, isActive: true }).exec();
    if (stateList.length) {
        return res.json({
            success: true,
            result: stateList,
            message: "Successfully fetched.."
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No data"
        })
    }

}



//Create a Bed
async function createNewbed(singleRoom, room_id, pg_id, value, res) {

    const saveBedDetails = new bedSchema({
        pg_id: pg_id,
        room_id: room_id,
        bedNumber: singleRoom.bedNumber,
        booked: singleRoom.booked,
        created_at: Date.now()
    })

    const savebed = await saveBedDetails.save();
    console.log(savebed)
    if (savebed) {
        if (value == true) {
            return res.json({
                success: true,
                pg_id: savebed.pg_id,
                room_id: savebed.room_id,
                message: "Pg Created SuccessFully",
            });
        }
    } else {
        return res.json({
            success: false,
            message: "pg not created"
        })
    }
}

exports.EditPG = async (req, res, next) => {
    try {

        const editPgData = {};
        editPgData["pgOwnerName"] = req.body.pgOwnerName;
        editPgData["pgNumber"] = req.body.pgNumber;
        editPgData["pgOwnerEmail"] = req.body.pgOwnerEmail;
        editPgData["pgOwnerNumber"] = req.body.pgOwnerNumber;
        editPgData["pgName"] = req.body.pgName;
        editPgData["state"] = req.body.state;
        editPgData["city"] = req.body.city;
        editPgData['landmark'] = req.body.landmark;
        editPgData["facilities"] = req.body.facilities;
        editPgData["addressLine1"] = req.body.addressLine1;
        editPgData["addressLine2"] = req.body.addressLine2;
        editPgData["thumbnail"] = req.body.thumbnail;
        editPgData["pgImage"] = req.body.pgImage;
        editPgData["isActive"] = req.body.isActive;
        editPgData["created_at"] = req.body.created_at
        const findandUpdatePg = await pgSchema.findByIdAndUpdate({ _id: req.body.pgId }, {
            $set: editPgData
        }, { new: true }
        )
        if (findandUpdatePg) {
            return res.json({
                success: true,
                result: editPgData,
                message: "Pg details are edited"
            })
        } else {
            return res.json({
                success: false,
                message: "No pg found"
            })
        }
    } catch (err) {
        return res.json({
            success: false,
            message: "Error occured" + error
        })
    }
}

module.exports.editServices = async (req, res, next) => {
    try {
        console.log(1)
        const findServiceSchema = await activeServicePgData.findByIdAndUpdate({ _id: req.body.serviceId }, {
            $set: {
                services: req.body.services
            }
        })
        if (findServiceSchema) {
            return res.json({
                success: true,
                message: "services Updated"
            })
        } else {
            return res.json({
                success: false,
                message: " service Not updated"
            })
        }
    } catch (err) {
        return res.json({
            success: false,
            message: "error  occurred" + error
        })
    }
}

module.exports.editMenuList = async (req, res, next) => {
    const findMenuDetails = await menuSchema.findByIdAndUpdate({ _id: req.body.menuId }, {
        $set: {
            breakfast: req.body.breakfast,
            lunch: req.body.lunch,
            dinner: req.body.dinner
        }
    }, { new: true }
    )
    if (findMenuDetails) {
        return res.json({
            success: true,
            message: "menu list updated successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "menu list not updated"
        })
    }
}


module.exports.editRoomDetails = async (req, res, next) => {
    try {
        const bedId = req.body.bedId
        let beds = req.body.bedDetails;
        console.log(beds)
        let editRoom = {};
        editRoom["roomNumber"] = req.body.roomNumber,
            editRoom["sharing"] = req.body.numberOfBeds,
            editRoom["basicRentPerMonth"] = req.body.basicRentPerMonth,
            editRoom["roomDeposit"] = req.body.roomDeposit,
            editRoom["floorNo"] = req.body.floorNo,
            editRoom["roomType"] = req.body.roomType,
            editRoom["facilities"] = req.body.facilities;
        editRoom["roomImage"] = req.body.roomImage,
            editRoom["created_at"] = Date.now()

        const findRoomDetails = await roomSchema.findByIdAndUpdate({
            _id: req.body.roomId
        }, {
            $set: editRoom
        }, { new: true }
        )
        if (findRoomDetails) {
            beds.forEach((bedEdit, i) => {
                console.log(beds.length)
                console.log(i)
                if (i + 1 == beds.length) {
                    console.log(i + 1 == beds.length)
                    EditBed(bedEdit, bedId, true, res)
                    console.log(1)
                }
                else {
                    EditBed(bedEdit, bedId, false, res)
                }
            })
        }
        else {
            return res.json({
                success: false,
                message: "Not Able to Edit Room"
            })
        }
    }
    catch (err) {
        return res.json({
            success: false,
            message: "error occured" + error
        })
    }
}

async function EditBed(singleRoom, bedId, value, res) {


    console.log(bedId)
    const EditBed = await bedSchema.findByIdAndUpdate({
        _id: bedId
    }, {
        $set: {
            bedNumber: singleRoom.bedNumber,
            booked: singleRoom.booked,
            created_at: Date.now()
        }
    })

    console.log(EditBed)
    const savebed = await EditBed.save();
    console.log(savebed)
    if (savebed) {
        if (value == true) {
            return res.json({
                success: true,
                room_id: savebed.room_id,
                message: "Room Updated SuccessFully",
            });
        }
    } else {
        return res.json({
            success: false,
            message: "Room not updated"
        })
    }
}




module.exports.getPgDetails = async (req, res, next) => {
    const getPg = await pgSchema.findOne({ _id: req.query.pgId })
    if (getPg) {
        return res.json({
            success: true,
            result: getPg,
            message: "pg details fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "not able to fetch data"
        })
    }
}


module.exports.getServices = async (req, res, next) => {
    const serviceDetails = await activeServicePgData.findOne({ pg_id: req.query.pgId }).exec();
    if (serviceDetails) {
        return res.json({
            success: true,
            result: serviceDetails,
            message: "service are fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "not able to fetch services"
        })
    }
}




module.exports.getMenuLists = async (req, res, next) => {
    const menuDetails = await menuSchema.findOne({ pg_id: req.query.pgId });
    if (menuDetails) {
        return res.json({
            success: true,
            result: menuDetails,
            message: "menu lists fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Not able to fetch the data"
        })
    }
}

module.exports.getRoomData = async (req, res, next) => {
    const roomId = req.query.roomId
    const RoomData = await roomSchema.findOne({ _id: roomId })
    if (RoomData) {
        getBed(roomId, RoomData, res)
    } else {
        return res.json({
            success: false,
            message: "Not able to fetch data"
        })
    }
}

async function getBed(roomId, RoomData, res) {
    const bedDetails = await bedSchema.find({ room_id: roomId })
    console.log(bedDetails)
    if (bedDetails) {

        return res.json({
            success: true,
            result: RoomData, bedDetails,
            message: "Room details fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Not able to ftech data"
        })
    }
}

// @desc create New pG offer
// @route post api/v1/addOfferPg
// access public
module.exports.addOfferPg = async (req, res, next) => {
    const offerId = req.body.offer_id
    if (offerId.length) {
        offerId.forEach(async (Id, i) => {
            const data = new addOfferSchema({
                pg_id: req.body.pg_id,
                offer_id: Id,
            });
            const saveData = await data.save();
            if (i + 1 == offerId.length) {
                return res.json({
                    success: true,
                    message: "Created Successfully!"
                });
            }
        })
    }
    else {
        return res.json({
            success: false,
            message: "offer_id is Empty"
        });
    }
}

// @desc check Number available PG
// @route get api/v1/checkNumber
// access public
module.exports.checkNumber = async (req, res, next) => {
    const number = req.query.num
    const RoomData = await roomSchema.find({ _id: number })
    if (RoomData.length) {
        return res.json({
            success: true,
            message: "Number Not Available"
        })
    } else {
        return res.json({
            success: false,
            message: "Number Available"
        })
    }
}