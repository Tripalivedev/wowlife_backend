const state = require("../../models/admin/state");
const pgDetails = require("../../models/admin/pgSchema")
const mongoose = require("mongoose");

module.exports.createStateList = async (req, res, next) => {

   var stateObj = [
      {
         "stateName": "Andhra Pradesh",
         "_id": "101"
      },
      {
         "stateName": "Arunachal Pradesh",
         "_id": "102"
      },
      {
         "stateName": "Assam",
         "_id": "103"
      },
      {
         "stateName": "Bihar",
         "_id": "104"
      },
      {
         "stateName": "Chandigarh (UT)",
         "_id": "105"
      },
      {
         "stateName": "Chhattisgarh",
         "_id": "106"
      },
      {
         "stateName": "Dadra and Nagar Haveli (UT)",
         "_id": "107"
      },
      {
         "stateName": "Daman and Diu (UT)",
         "_id": "108"
      },
      {
         "stateName": "Delhi (NCT)",
         "_id": "109"
      },
      {
         "stateName": "Goa",
         "_id": "110"
      },
      {
         "stateName": "Gujarat",
         "_id": "111"
      },
      {
         "stateName": "Haryana",
         "_id": "112"
      },
      {
         "stateName": "Himachal Pradesh",
         "_id": "113"
      },
      {
         "stateName": "Jammu and Kashmir",
         "_id": "114"
      },
      {
         "stateName": "Jharkhand",
         "_id": "115"
      },
      {
         "stateName": "Karnataka",
         "_id": "116"
      },
      {
         "stateName": "Kerala",
         "_id": "117"
      },
      {
         "stateName": "Lakshadweep",
         "_id": "118"
      },
      {
         "stateName": "Madhya Pradesh",
         "_id": "119"
      },
      {
         "stateName": "Maharashtra",
         "_id": "120"
      },
      {
         "stateName": "Manipur",
         "_id": "121"
      },
      {
         "stateName": "Meghalaya",
         "_id": "122"
      },
      {
         "stateName": "Mizoram",
         "_id": "123"
      },
      {
         "stateName": "Nagaland",
         "_id": "124"
      },
      {
         "stateName": "Odisha",
         "_id": "125"
      },
      {
         "stateName": "Puducherry",
         "_id": "126"
      },
      {
         "stateName": "Punjab",
         "_id": "127"
      },
      {
         "stateName": "Rajasthan",
         "_id": "128"
      },
      {
         "stateName": "Sikkim",
         "_id": "129"
      },
      {
         "stateName": "Tamil Nadu",
         "_id": "130"
      },
      {
         "stateName": "Telangana",
         "_id": "131"
      },
      {
         "stateName": "Tripura",
         "_id": "132"
      },
      {
         "stateName": "Uttarakhand",
         "_id": "133"
      },
      {
         "stateName": "Uttar Pradesh",
         "_id": "134"
      },
      {
         "stateName": "West Bengal",
         "_id": "135"
      },
      {
         "stateName": "Andaman and Nicobar Island",
         "_id": "136"
      }
   ];

   await state.create(stateObj);

}

module.exports.getStateList = async (req, res, next) => {

   const stateList = await state.find().exec();
   if (stateList.length) {
      return res.json({
         success: true,
         result: stateList,
         message: "Successfully fetched.."
      })
   }
   else {
      return res.json({
         success: false,
         result: [],
         message: "No data"
      })
   }
}


module.exports.addState = async (req, res, next) => {

   let { stateName, customerCare, phone, uploadPhoto } = req.body;
   //updateState
   const updateState = await state.findOneAndUpdate(
      { stateName: stateName },
      {
         $set: {
            customerCare: customerCare,
            phone: phone,
            uploadPhoto: uploadPhoto,
            isActive: true
         }
      },
      { new: true }
   );
   console.log(updateState)



   if (updateState) {
      return res.json({
         success: true,
         message: "Successfully updated state.."
      })
   }
   else {
      return res.json({
         success: false,
         message: "Error occured"
      })
   }

}

module.exports.getPgByState = async(req,res,next)=>{
   const PgDetails = await pgDetails.find({state:req.query.stateName});
   if(PgDetails){
      return res.json({
         success:true,
         result:PgDetails,
         message:"Pg fetched successfully"})
   }else{
      return res.json({
         success:false,
      message:"No Data found"})
   }
}