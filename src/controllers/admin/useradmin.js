const UserAdmin = require("../../models/admin/useradmin");
const Users = require("../../models/application/user");
const state = require("../../models/admin/state");
const city = require("../../models/admin/city");
const pgSchema = require("../../models/admin/pgSchema");
const complaintSchema = require("../../models/application/complaintsUser")
const TicketRequests = require("../../models/application/ticketRequest")

const { sendNotification } = require("../../helpers/notification");
const notificationSchema = require("../../models/application/notification");
const { addNotification } = require("../application/notification");
const bedSchema = require("../../models/admin/bedDetails")


module.exports.register = async (req, res, next) => {

    // let {userName,password} = req.body;
    // const data = new Users({userName: userName,password: password});
    // const saveData = await data.save();
    // if(saveData)
    // {
    //     return res.json({
    //         success: true,
    //         message: "Successfully registered.."
    //     })
    // }
    // else
    // {
    //     return res.json({
    //         success: false,
    //         message: "Error Occured"
    //     })
    // }

}

module.exports.login = async (req, res, next) => {

    let { userName, password } = req.body;
    const userInfo = await UserAdmin.findOne({ userName: userName, password: password }).exec();
    if (userInfo) {
        return res.json({
            success: true,
            message: "Successfully loggedIn.."
        })
    }
    else {
        return res.json({
            success: false,
            message: "Invalid Credentials"
        })
    }

}

module.exports.updateUserDetails = async (req, res, next) => {

    let { userName, email, gender, user_id } = req.body;
    const updateDetail = await Users.findOneAndUpdate(
        { _id: user_id },
        {
            $set: {
                userName: userName,
                email: email,
                gender: gender
            }
        },
        { new: true }
    );
    if (updateDetail) {
        return res.json({
            success: true,
            message: "Successfully Updated.."
        })
    }
    else {
        return res.json({
            success: false,
            message: "Error occured.."
        })
    }
}
// @desc get all users
// @route get api/v1/fetchUser
// access public
module.exports.fetchUser = async (req, res, next) => {
    const getUser = await Users.find();
    if (getUser.length) {
        return res.json({
            success: true,
            result: getUser,
            message: "All users get successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "error in fetching User details"
        })
    }
}

// @desc single User
// @route get api/v1/fetchUserById
// access public
exports.fetchUserById = async (req, res, next) => {
    const userDetails = await Users.findById({ _id: req.query.userId })
    if (userDetails) {
        return res.json({
            success: true,
            result: userDetails,
            message: "User details fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "error in fetching user details"
        })
    }
}

// @desc dashboard
// @route get api/v1/dashboard
// access public
exports.dashboard = async (req, res, next) => {
    try {
        let totalowner = await pgSchema.find();
        let totalBed = await bedSchema.find()
        let vaccancyBed = await bedSchema.find({ booked: false })
        let bookedBed = await bedSchema.find({ booked: true })

        //    "totalState": (totalState.length == undefined ? 0 : totalState.length),
        //     "totalUsers": (totalUsers.length == undefined ? 0 : totalUsers.length),
        //     "totalcity": (totalcity.length == undefined ? 0 : totalcity.length),
        let dashboardData = {
            "totalPg": (totalowner.length == undefined ? 0 : totalowner.length),
            "totalBed": (totalBed.length == undefined ? 0 : totalBed.length),
            "totalVaccancyBed": (vaccancyBed.length == undefined ? 0 : vaccancyBed.length),
            "totalBookedBed": (bookedBed.length == undefined ? 0 : bookedBed.length)


        }
        if (dashboardData) {
            return res.json({
                success: true,
                result: dashboardData,
                message: "Data Fetched successfully"
            })
        } else {
            return res.json({
                success: false,
                message: "error - fetching Data"
            })
            1
        }

    } catch (error) {
        return res.json({
            success: false,
            message: "error occured" + error
        })
    }
}



//getComplaintsofAll

module.exports.getComplaintsforAdmin = async (req, res) => {
    const complaintDetails = await complaintSchema.find().populate("user_id", "userName").populate("pg_id", "pgName pgOwnerName pgOwnerEmail pgOwnerNumber").exec();
    if (complaintDetails) {
        return res.json({
            success: true,
            result: complaintDetails,
            message: "Complaints are fetched"
        })
    } else {
        return res.josn({
            success: false,
            message: "No complaints found"
        })
    }

}



module.exports.updateStatusOfComplaint = async (req, res, next) => {

    const complaintDetails = await complaintSchema.findOne({ _id: req.body.complaintId })

    if (complaintDetails.status == "Completed") {
        return res.json({
            success: true,
            message: "Status already Completed"
        })
    } else {
        const updateComplaints = await complaintSchema.findByIdAndUpdate({ _id: req.body.complaintId },
            {
                $set: {
                    status: "Completed"
                }
            }, { new: true })

        if (updateComplaints) {

            addNotification(updateComplaints.user_id, 1);

            sendNotification(updateComplaints.user_id, 1);

            return res.json({
                success: true,
                message: "Status chnaged and notification SEnt"
            });

        }
        else {
            return res.json({
                success: false,
                message: "not able to cahnge the status"
            })
        }
    }
}


exports.getUserRequests = async (req, res, next) => {

    let ticketData = await TicketRequests.find().populate("user_id" ,"userName email bookedBedId phone").populate("pg_id" ,"pgName pgOwnerName pgNumber pgOwnerNumber").exec();
    if (ticketData.length) {
        return res.json({
            success: true,
            result: ticketData,
            message: "Successfully fetched"
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No tickets"
        })
    }

}

module.exports.addComment = async(req,res,next)=>{
    const requestId  = req.body.requestId;
    const forComment = await TicketRequests.findOneAndUpdate({
        _id:requestId
    },
    {
    $set:{
        comment:req.body.comment
    }
},{new:true})
console.log(forComment.user_id)
if(forComment){
    addNotification(forComment.user_id, 2);

            sendNotification(forComment.user_id, 2);

            return res.json({
                success: true,
                message: "Comment Added ad Notification Sent"
            });

        }
        else {
            return res.json({
                success: false,
                message: "not able to add comment"
            })
        }
}

module.exports.updateStatusOfRequests = async (req, res, next) => {

    const requestDetails = await TicketRequests.findOne({ _id: req.body.requestId })

    if (complaintDetails.status == "Completed") {
        return res.json({
            success: true,
            message: "Status already Completed"
        })
    } else {
        const updateRequests= await TicketRequests.findByIdAndUpdate({ _id: req.body.requestId },
            {
                $set: {
                    status: "Completed"
                }
            }, { new: true })

        if (updateRequests) {

            addNotification(updateRequests.user_id, 1);

            sendNotification(updateRequests.user_id, 1);

            return res.json({
                success: true,
                message: "Status chnaged and notification SEnt"
            });

        }
        else {
            return res.json({
                success: false,
                message: "not able to cahnge the status"
            })
        }
    }
}