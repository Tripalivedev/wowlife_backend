const enquirySchema = require("../../models/application/enquiry")
const complaintsSchema = require("../../models/admin/complaints")
const complaintsFeatureSchema = require("../../models/admin/complaintsFeature")

// @desc enquiry form
// @route post api/v1/submit_enquiry
// access public
module.exports.submit_enquiry = async (req, res, next) => {
    try {
        let enquiryData = new enquirySchema({
            ownerName: req.body.ownerName,
            phoneNumber: req.body.phone,
            email: req.body.email,
            hostelName: req.body.hostel,
            numberofBeds: req.body.numberofBeds,
            buildingRent: req.body.buildingRent,
            buildingSquareFeet: req.body.buildingSquareFeet,
            facility: req.body.facility,
            createdAt: Date.now()
        })
        await enquiryData.save();
        if (enquiryData) {
            return res.json({
                success: true,
                data: enquiryData,
                message: "Enquiry registered",
            });
        }
        else {
            return res.json({
                success: false,
                message: "Error in save enquiry"
            })
        }

    } catch (error) {
        return res.json({
            success: false,
            message: "Error occured" + error,
        });
    }
}


// @desc get enquiry form
// @route get api/v1/get_enquiry
// access public
module.exports.get_enquiry = async (req, res, next) => {
    try {
        let enquiryData = await enquirySchema.find()
        if (enquiryData.length) {
            return res.json({
                success: true,
                data: enquiryData,
                message: "Enquiry registered",
            });
        }
        else {
            return res.json({
                success: false,
                message: "No Enquiry Find"
            })
        }

    } catch (error) {
        return res.json({
            success: false,
            message: "Error occured" + error,
        });
    }

}

// @desc get all Complaints
// @route get api/v1/getAllComplaints
// access public
module.exports.getAllComplaints = async (req, res, next) => {
    console.log('ss')
    let getAllComplaints = await complaintsSchema.find();
    if (getAllComplaints.length) {
        return res.json({
            success: true,
            data: getAllComplaints,
            message: "Get complaints SuccessFully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "complaints is Empty",
        });
    }

}

// @desc get all Complaints
// @route get api/v1/getSingleComplaints
// access public
module.exports.getSingleComplaints = async (req, res, next) => {

    let getSingleComplaints = await complaintsFeatureSchema.find();
    if (getSingleComplaints.length) {
        return res.json({
            success: true,
            data: getSingleComplaints,
            message: "Get complaints SuccessFully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "complaints is Empty",
        });
    }

}

