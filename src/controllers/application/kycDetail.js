const kycDetail = require("../../models/application/kycDetail");

// @desc create KYC
// @route post api/v1/createKycDetails
// access public
module.exports.createKycDetails = async (req, res, next) => {

    let {
        aadharCardImage,
        aadharCardNo,
        idproofImage,
        idproofNo,
        DOB,
        permanentAddress1,
        permanentAddress2,
        occupationDetail,
        localGuardianPhoneNo,
        localGuardianAddress,
        acknowledgeAndEmergency } = req.body;

    const data = new kycDetail({
        user_id: req.user_id,
        aadharCardImage: aadharCardImage,
        aadharCardNo: aadharCardNo,
        idproofImage: idproofImage,
        idproofNo: idproofNo,
        DOB: DOB,
        permanentAddress1: permanentAddress1,
        permanentAddress2: permanentAddress2,
        occupationDetail: occupationDetail,
        localGuardianPhoneNo: localGuardianPhoneNo,
        localGuardianAddress: localGuardianAddress,
        acknowledgeAndEmergency: acknowledgeAndEmergency
    });
    const saveData = await data.save();
    if (saveData) {
        return res.json({
            success: true,
            message: "Successfully updated.."
        })
    }
    else {
        return res.json({
            success: false,
            message: "Error Occured"
        })
    }

}