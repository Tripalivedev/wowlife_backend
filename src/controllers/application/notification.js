const { title } = require("process");
const Notification = require("../../models/application/notification");

//addNotification
exports.addNotification = async function(sender,type,message,title){

    const data = new Notification({
        user_id: sender,
        type: type,
        title:title,
        message:message,
        seen:false
    });

    await data.save();

}

//getAllNotification
module.exports.getAllNotification = async(req,res,next)=>{

    let user_id = req.query.user_id;

    const getNotification = await Notification.find({sender_id:user_id}).populate("sender_id","userName").exec();

    if(getNotification.length)
    {
        return res.json({
            success : true,
            result : getNotification,
            message: "Notifications fetched successfully"
        })
    }
    else
    {
        return res.json({
            success : true,
            result : getNotification,
            message: "No notifications"
        })
    }

}

//getUnreadCount
exports.getUnreadCount = async(req,res,next)=>{
   
        let sender_id = req.user_id;
        const getUnreadData = await Notification.find({sender_id: sender_id,seen:false}).exec();
        var count = getUnreadData.length;
        if(getUnreadData.length)
        {
            return res.json({
                success:true,
                count:count
            })
        }
        else
        {
            return res.json({
                success:false,
                count: "",
                message:"No unread message"
            }) 
        }
    
}

//updateReadCount
exports.updateReadCount = async(req,res,next)=>{

        const notifyId = req.body.notifyId;
        const saveRead = await Notification.updateOne(
            {_id:notifyId},
            {
                $set:{seen:true}
            },
            {new : true}
        );
        if(saveRead)
        {
            return res.json({
                success:true,
                message:"Updated notification seen"
            }) 
        }
        else
        {
            return res.json({
                success:false,
                message:"Error occured"+error
            })
        }

}