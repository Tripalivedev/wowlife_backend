const favouriteSchema = require("../../models/application/favourite")
const pgServiceSChema = require("../../models/admin/activeServicePgData")
const pgSchema = require("../../models/admin/pgSchema")
const ratingSchema = require("../../models/application/rating")
const roomSchema = require("../../models/admin/roomDetails")
const savedPlaceSchema = require("../../models/application/savedPlaces")
const bedSchema = require("../../models/admin/bedDetails")
const offerPgSchema = require("../../models/admin/offerPg");
const menuSchema = require("../../models/admin/foodMenuList");
const offerMaster = require("../../models/admin/offerMaster")
// @desc popular Places
// @route get api/v1/popularPlaces
// access public
module.exports.popularPlaces = async (req, res, next) => {

    const allPgList = await pgSchema.find({
        state: req.query.state,
        city: req.query.city
    });
    if (allPgList.length) {
        let arrData = []
        allPgList.forEach((element, i) => {
            arrData.push(element.landmark)
            if (i + 1 == allPgList.length) {
                const counts = {};
                arrData.forEach(function (x) { counts[x] = (counts[x] || 0) + 1; });
                return res.json({
                    success: true,
                    data: Object.keys(counts),
                    message: "Listed Successfully"
                })
            }
        });
    }
    else {
        return res.json({
            success: false,
            message: "pg List is Empty"
        })
    }
}


// @desc popular Places pg list
// @route get api/v1/popularPlacesPg
// access public
module.exports.popularPlacesPg = async (req, res, next) => {

    let user_id = req.user_id;
    var findAreaPg;
    if (req.query.landmark !== "all") {
        findAreaPg = await pgSchema.find({
            state: req.query.state,
            city: req.query.city,
            landmark: req.query.landmark,
        });
    }
    else {
        findAreaPg = await pgSchema.find({
            state: req.query.state,
            city: req.query.city,
        });
    }

    if (findAreaPg.length) {
        let favouritePg = await favouriteSchema.distinct("pg_id.pgId", { user_id: user_id }).exec();
        favouritePg = favouritePg.map(String);

        if (favouritePg.length) {
            var count = 0;
            let totalLength = favouritePg.length * findAreaPg.length;

            findAreaPg.forEach((data) => {
                favouritePg.forEach((id) => {
                    if (id == data._id) {
                        data.isFavourite = true;
                    }
                    count = count + 1;
                    if (totalLength == count) {
                        Response();
                    }
                })
            })
        }
        else {
            Response();
        }

        function Response() {
            return res.json({
                success: true,
                landmark: (req.query.landmark == undefined ? "all" : req.query.landmark),
                data: findAreaPg,
                message: "Listed Successfully"
            })
        }
    }
    else {
        return res.json({
            success: false,
            message: "pg List is Empty"
        })
    }
}

// @desc find landmark list using city
// @route get api/v1/getLandMark
// access public
module.exports.getLandMark = async (req, res, next) => {

    const findLandMark = await pgSchema.distinct("landmark", {
        state: req.query.state,
        city: req.query.city
    }).exec();
    if (findLandMark.length) {
        return res.json({
            success: true,
            data: findLandMark,
            message: "landmark Listed Successfully"
        })
    }
    else {
        return res.json({
            success: false,
            message: "No Landmark find for this city"
        })
    }
}


// @desc search using address and pg name
// @route get api/v1/searchMain
// access public
module.exports.searchMain = async (req, res, next) => {
    const search = req.query.pgName;
    let get_data = await pgSchema.find({
        state: req.query.state,
        city: req.query.city,
    });

    if (get_data.length) {
        const all_users = get_data.filter(data => new RegExp(search, "ig").test(data.pgName)).sort((a, b) => {
            let re = new RegExp("^" + search, "i")
            return re.test(a.pgName) ? re.test(b.pgName) ? a.pgName.localeCompare(b.pgName) : -1 : 1
        })

        if (all_users.length) {
            return res.json({
                success: true,
                data: all_users,
                message: "Fetched Successfully"
            })
        }
        else {
            return res.json({
                success: false,
                message: "Unavailable"
            })
        }
    }
    else {
        return res.json({
            success: false,
            message: "No Pg Found"
        })
    }
}


// @desc main Filter
// @route get api/v1/filterMain
// access public
module.exports.filterMain = async (req, res, next) => {

    let user_id = req.user_id;
    let fac = req.body.facilities;
    let maxAmount = req.body.maxAmount;
    let minAmount = req.body.minAmount;
    let sharing = req.body.sharing;
    let roomType = req.body.roomType;

    const get_room = await roomSchema.distinct("pg_id",
        {
            sharing: sharing,
            basicRentPerMonth: { $gte: minAmount, $lte: maxAmount },
            roomType: roomType,
            facilities: { $elemMatch: { name: fac } }
        }).exec();

    if (get_room.length) {
        let allPgList = await pgSchema.find({
            _id: get_room,
            isActive: true,
            state: req.body.state,
            city: req.body.city
        });

        if (allPgList.length) {
            let favouritePg = await favouriteSchema.distinct("pg_id.pgId", { user_id: user_id }).exec();
            favouritePg = favouritePg.map(String);

            if (favouritePg.length) {
                var count = 0;
                let totalLength = favouritePg.length * allPgList.length;

                allPgList.forEach((data) => {
                    favouritePg.forEach((id) => {
                        if (id == data._id) {
                            data.isFavourite = true;
                        }
                        count = count + 1;
                        if (totalLength == count) {
                            Response();
                        }
                    })
                })
            }
            else {
                Response();
            }

            function Response() {
                let result = allPgList.reduce(function (r, a) {
                    r[a.landmark] = r[a.landmark] || [];
                    r[a.landmark].push(a);
                    return r;
                }, Object.create(null));
                if (result) {
                    return res.json({
                        success: true,
                        result: result,
                        message: "fetched Successfully"
                    });
                }
            }
        }
        else {
            return res.json({
                success: false,
                result: [],
                message: "No Pg Found"
            })
        }

    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No Pg Found"
        })
    }
}

// @desc rate pg by user
// @route post api/v1/ratePg
// access public
module.exports.ratePg = async (req, res, next) => {

    const createRating = new ratingSchema({
        user_id: req.user_id,
        pg_id: req.body.pgId,
        rating: req.body.rating,
        created_at: Date.now()
    });
    const save_Data = await createRating.save();
    if (save_Data) {
        //Get all rating
        const rating_data = await ratingSchema.distinct("rating", {
            pg_id: req.body.pgId,
        });
        if (rating_data.length) {
            var res = rating_data.reduce(function (prev, curr) {
                return (Number(prev) || 0) + (Number(curr) || 0);
            });
            var total_users_rated = Number(rating_data.length)
            var sum_of_max_rating_of_user_count = total_users_rated * 5
            var sum_of_rating = res
            const average_Rating = ((Number(sum_of_rating) * 5) / sum_of_max_rating_of_user_count).toFixed(2);

            const updateRating = await pgSchema.findOneAndUpdate(
                { _id: req.body.pgId },
                {
                    $set: {
                        rating: average_Rating.toString()
                    }
                },
                { new: true }
            );
            if (updateRating) {
                return res.json({
                    success: true,
                    message: "Rated Successfully"
                })
            }
        }
        else {
            return res.json({
                success: false,
                message: "Not able to update rating"
            })
        }
    }
    else {
        return res.json({
            success: false,
            message: "Not able to update rating"
        })
    }
}

// @desc get one pg
// @route get api/v1/getOnePg
// access public
module.exports.getOnePg = async (req, res, next) => {

    let user_id = req.query.user_id;
    const pgId = req.query.pgId
    let today = new Date().toISOString().slice(0, 10)
    // let getDate = today.getFullYear();
    //console.log(today);
    const array = [];
    const get_Pg = await pgSchema.findOne({ _id: pgId, isActive: true }).exec();
    const get_offers = await offerPgSchema.find({ pg_id: pgId })
    const get_offerMaster = await offerMaster.distinct("expireDate", {
        offer_id: get_offers.offer_id
    })
    get_offerMaster.forEach(async (data) => {
        console.log(data)
        if (data < today) {

            const get_offers2 = await offerPgSchema.findOneAndUpdate({ pg_id: req.query.pgId }, {
                $set: {
                    isActive: false
                }
            }, { new: true })



        }
    })
    const get_offerSchema = await offerPgSchema.find({ pg_id: pgId, isActive: true }).populate("offer_id", "name percentage code expireDate image").exec();
    if (get_Pg) {
        let favouritePg = await favouriteSchema.distinct("pg_id.pgId", { user_id: user_id }).exec();
        favouritePg = favouritePg.toString();

        if (favouritePg.includes(pgId.toString())) {
            get_Pg.isFavourite = true;
        }


        return res.json({
            success: true,
            pg: get_Pg,
            offerData: get_offerSchema,
            message: "Fetched Successfully"
        })
    }
    else {
        return res.json({
            success: false,
            message: "No Pg Found"
        })
    }
}

// @desc Add Favourite
// @route POST api/v1/addFavourite
// access public
module.exports.addFavourite = async (req, res, next) => {
    let addFavouriteSchema = await favouriteSchema.findOne({
        user_id: req.user_id,
    })
    if (addFavouriteSchema) {
        console.log(addFavouriteSchema._id)
        const editFavourite = await favouriteSchema.findByIdAndUpdate(
            { _id: addFavouriteSchema._id },
            {
                $push: {
                    "pg_id": { pgId: req.body.pgId }
                }

            }, { new: true }
        ).exec()
        console.log(editFavourite)
        if (editFavourite) {
            return res.json({
                success: true,
                message: "Added Successfully"
            })
        }
        else {
            return res.json({
                success: false,
                message: "error occured"
            })
        }
    }
    else {
        const createFavourite = new favouriteSchema({
            user_id: req.user_id,
            pg_id: [{
                pgId: req.body.pgId
            }],
        }).save();
        if (createFavourite) {
            return res.json({
                success: true,
                message: "Added Successfully"
            })
        }
        else {
            return res.json({
                success: false,
                message: "error occured"
            })
        }
    }
}

// @desc get Favourite
// @route get api/v1/getFavourite
// access public
module.exports.getFavourite = async (req, res, next) => {

    let user_id = req.user_id;
    let getFavouriteSchema = await favouriteSchema.distinct("pg_id", {
        user_id: req.user_id,
    });

    if (getFavouriteSchema.length) {
        let arr = [];
        getFavouriteSchema.forEach(async (data, i) => {
            arr.push(data.pgId)
            if (i + 1 == getFavouriteSchema.length) {
                const allPgList = await pgSchema.find({
                    _id: arr
                });
                if (allPgList.length) {
                    let favouritePg = await favouriteSchema.distinct("pg_id.pgId", { user_id: user_id }).exec();
                    favouritePg = favouritePg.map(String);

                    if (favouritePg.length) {
                        var count = 0;
                        let totalLength = favouritePg.length * allPgList.length;

                        allPgList.forEach((data) => {
                            favouritePg.forEach((id) => {
                                if (id == data._id) {
                                    data.isFavourite = true;
                                }
                                count = count + 1;
                                if (totalLength == count) {
                                    Response();
                                }
                            })
                        })
                    }
                    else {
                        Response();
                    }

                    function Response() {
                        return res.json({
                            success: true,
                            result: allPgList,
                            message: "Fetched Successfully"
                        })
                    }

                }
                else {
                    return res.json({
                        success: false,
                        message: "No Data Found"
                    })
                }
            }
        })
    }
    else {
        return res.json({
            success: false,
            message: "No Data Found"
        })
    }
}

// @desc delete Favourite
// @route get api/v1/removeFavourite
// access public
module.exports.removeFavourite = async (req, res, next) => {
    const removeFav = await favouriteSchema.updateOne(
        { user_id: req.user_id },
        {
            $pull: {
                "pg_id": {
                    pgId: req.query.pgId
                }
            }
        }, { new: true }
    ).exec();
    if (removeFav) {
        return res.json({
            success: true,
            message: "Removed Successfully"
        })
    }
    else {
        return res.json({
            success: false,
            message: "error occured"
        })
    }
}

// @desc Add saved places
// @route POST api/v1/addSavedPlace
// access public
module.exports.addSavedPlace = async (req, res, next) => {
    let getSavedData = await savedPlaceSchema.findOne({
        user_id: req.user_id,
    })
    if (getSavedData) {
        console.log(getSavedData._id)
        const editSchema = await savedPlaceSchema.findByIdAndUpdate(
            { _id: getSavedData._id },
            {
                $push: {
                    "places": { title: req.body.title, lat:req.body.lat, lang :req.body.lang}
                }

            }, { new: true }
        ).exec()
        console.log(editSchema)
        if (editSchema) {
            return res.json({
                success: true,
                message: "Added Successfully"
            })
        }
        else {
            return res.json({
                success: false,
                message: "error occured"
            })
        }
    }
    else {
        const createSavedData = new savedPlaceSchema({
            user_id: req.user_id,
            places: [{
                title: req.body.title, lat:req.body.lat, lang :req.body.lang
            }],
        }).save();
        if (createSavedData) {
            return res.json({
                success: true,
                message: "Added Successfully"
            })
        }
        else {
            return res.json({
                success: false,
                message: "error occured"
            })
        }
    }
}

// @desc get saved places
// @route get api/v1/getSavedPlace
// access public
module.exports.getSavedPlace = async (req, res, next) => {
    console.log(req.user_id)
    let getSavedData = await savedPlaceSchema.findOne({
        user_id: req.user_id,
    })
    console.log(getSavedData)
    if (getSavedData) {
        return res.json({
            success: true,
            result: getSavedData,
            message: "Fetched Successfully"
        })
    }
    else {
        return res.json({
            success: false,
            message: "No Data Found"
        })
    }
}

// @desc get All State
// @route get api/v1/getState
// access public
module.exports.getState = async (req, res, next) => {
    const get_state = await pgSchema.distinct("state");
    if (get_state.length) {
        let state = [...new Set(get_state)];
        return res.json({
            success: true,
            result: state,
            message: "Get State Succeessfully"
        });
    }
    else {
        return res.json({
            success: false,
            message: "No State Found"
        });
    }

}

// @desc get All City
// @route get api/v1/getCity
// access public
module.exports.getCity = async (req, res, next) => {
    const get_City = await pgSchema.distinct("city", {
        state: req.query.state,
    });
    if (get_City.length) {
        let city = [...new Set(get_City)];
        return res.json({
            success: true,
            result: city,
            message: "Get City Succeessfully"
        });
    }
    else {
        return res.json({
            success: false,
            message: "No State Found"
        });
    }
}

// @desc get All near by pg list
// @route get api/v1/nearByPg
// access public
module.exports.nearByPg = async (req, res, next) => {

    let user_id = req.user_id;
    const allPgList = await pgSchema.find({
        state: req.query.state,
        city: req.query.city,
        isActive: true,
    }).exec()
    console.log(allPgList)
    if (allPgList.length) {
        let favouritePg = await favouriteSchema.distinct("pg_id.pgId", { user_id: user_id }).exec();
        favouritePg = favouritePg.map(String);

        if (favouritePg.length) {
            var count = 0;
            let totalLength = favouritePg.length * allPgList.length;

            allPgList.forEach((data) => {
                favouritePg.forEach((id) => {
                    if (id == data._id) {
                        data.isFavourite = true;
                    }
                    count = count + 1;
                    if (totalLength == count) {
                        Response();
                    }
                })
            })
        }
        else {
            Response();
        }

        function Response() {
            let result = allPgList.reduce(function (r, a) {
                r[a.landmark] = r[a.landmark] || [];
                r[a.landmark].push(a);
                return r;
            }, Object.create(null));
            if (result) {
                return res.json({
                    success: true,
                    result: result,
                    message: "fetched Successfully"
                });
            }
        }
    }
    else {
        return res.json({
            success: false,
            message: "No Data Found"
        });
    }
}

// @desc filter room for pg
// @route POST api/v1/pgRoomFilter
// access public
module.exports.pgRoomFilter = async (req, res, next) => {
    const { pgId, minAmount, maxAmount, sharingCount, facilities } = req.body;
    console.log("test123")
    const get_room = await roomSchema.find({
        pg_id: { $eq: pgId },
        sharing: sharingCount,
        basicRentPerMonth: { $gte: minAmount, $lte: maxAmount },
        facilities: { $elemMatch: { name: facilities } }
    }).exec();
    if (get_room) {
        return res.json({
            success: true,
            result: get_room,
            message: "Rooms fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Rooms not Available"
        })
    }
}

// @desc get all the data of the room
// @route get api/v1/getAllDataRoom
// access public
module.exports.getAllDataRoom = async (req, res, next) => {
    const { pgId, roomId } = req.query;
    console.log(pgId, roomId)
    const get_Pg = await bedSchema.findOne({
        pg_id: pgId,
        room_id: roomId,
        booked: false
    }).populate("pg_id", "pgOwnerName addressLine1 city state pgImage")
        .populate("room_id", "sharing facilities roomNumber roomDeposit basicRentPerMonth")
    const get_offers = await offerPgSchema.find({ pg_id: pgId }).populate("offer_id", "name percentage code expireDate image").exec();
    const getPgService = await pgServiceSChema.findOne({ pg_id: pgId })
    let result;
    var arr = []
    if (getPgService) {
        if (getPgService.services.length) {
            result = getPgService.services.reduce(function (r, a) {
                r[a.name] = r[a.name] || [];
                r[a.name].push(a);
                return r;
            }, Object.create(null));
        }
    }
    if (result != undefined) {
        var values = Object.values(result)
        if (values.length) {
            values.forEach((item, num) => {
                var objIn = {
                    "service": item[0].name,
                    "image": item[0].name,
                    "data": item,
                }
                arr.push(objIn)
            })
        }
    }
    if (get_Pg) {
        return res.json({
            success: true,
            pgData: get_Pg,
            services: (arr.length ? arr : "No services found"),
            offer: (get_offers.length ? get_offers : "no offer's found"),
            message: "Rooms fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Rooms not Available"
        })
    }
}

// @desc pg  menu
// @route get api/v1/pgMenu
// access public
module.exports.pgMenu = async (req, res, next) => {

    const pg_menu = await menuSchema.findOne({ pg_id: req.query.pgId }).exec();

    if (pg_menu) {
        return res.json({
            success: true,
            menu: pg_menu,
            message: "Fetched Successfully"
        })
    }
    else {
        return res.json({
            success: false,
            message: "No Menu"
        })
    }
}
