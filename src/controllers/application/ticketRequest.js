const TicketRequest = require("../../models/application/ticketRequest");
const complaintsSchema = require("../../models/admin/complaints")
const complaintsFeatureSchema = require("../../models/admin/complaintsFeature")
const {sendNotification} = require("../../helpers/notification");
const notificationSchema = require("../../models/application/notification");
const { addNotification } = require("../application/notification");
const userComplaintSchema = require("../../models/application/complaintsUser")


//createTicketRequest
exports.createTicketRequest = async (req, res, next) => {

    let { pg_id, amenities, description } = req.body;

    const ticket = new TicketRequest({
        pg_id: pg_id,
        user_id: req.user_id,
        amenities: amenities,
        description: description
    });

    const saveData = await ticket.save();
    if (saveData) {
        return res.json({
            success: true,
            result: saveData,
            id: saveData._id,
            message: "Successfully saved"
        })
    }
    else {
        return res.json({
            success: false,
            message: "Error occured"
        })
    }

}

//getAllTickets
exports.getAllTickets = async (req, res, next) => {

    let ticketData = await TicketRequest.find();
    if (ticketData.length) {
        return res.json({
            success: true,
            result: ticketData,
            message: "Successfully fetched"
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No tickets"
        })
    }

}

// @desc get complaints master
// @route get api/v1/getComplaintsApp
// access public
module.exports.getComplaintsApp = async (req, res, next) => {

    let complaintsData = await complaintsSchema.find();
    if (complaintsData.length) {
        return res.json({
            success: true,
            result: complaintsData,
            message: "Successfully fetched"
        })
    }
    else {
        return res.json({
            success: false,
            result: [],
            message: "No complaints"
        })
    }
}

// @desc get complaints master
// @route get api/v1/getComplaintsByIdApp
// access public
module.exports.getComplaintsByIdApp = async (req, res, next) => {
    const complaintData = await complaintsFeatureSchema.find({ comp_id: req.query.complaintId })
    if (complaintData.length) {
        return res.json({
            success: true,
            result: complaintData,
            message: "comaplints are fetched successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Please Add Data"
        })
    }
}

// @desc create user Complaints
// @route post api/v1/createUserComplaints
// access public
module.exports.createUserComplaints = async (req, res, next) => {
    var createUserComp = new userComplaintSchema({
        user_id: req.user_id,
        pg_id: req.body.pg_id,
        complaint_id: req.body.complaint_id,
        title: req.body.title,
        complaint_feature: req.body.complaint_feature,
        things: req.body.things,
        description: req.body.description,
        image: req.body.image,
        status: "Pending",
    })

    const saveData = await createUserComp.save();

    if(saveData){


        const updateNotification = new notificationSchema({
            user_id: req.user_id,
            type:0,
            seen: false,
            title :'New Complaint',
        message :' You have raised a complaint',
            created_at:Date.now()
            });
            const saveNotificationData = await updateNotification.save();
            if(saveNotificationData){
                sendNotification(req.user_id, 0);
                return res.json({
                    success:true,
                data:saveData,
            message:"Payment added "})
            }
        sendNotification(saveData.user_id, 0);

        return res.json({
            success: true,
            message: "Complaint Raised and notification sent"
        });

    }
    else {
        return res.json({
            success: false,
            message: "Failed Data Not Added",
        });
    }
}

module.exports.getComplaintsOnStatus = async (req, res, next) => {
    const user_id = req.user_id;
    const status = req.query.status

    const findStatus = await userComplaintSchema.find({ user_id: user_id})
    if (req.query.status  == "Pending") {
       const findStatusPending = await userComplaintSchema.find({status:status})
        return res.json({
            success: true,
            result: findStatusPending,
            message: "complaints are fetched"
        })
    } else if(req.query.status == "Completed") {
        const findStatusCompleted = await userComplaintSchema.find({status:status})
        return res.json({
            success: true,
            result: findStatusCompleted,
            message: "complaints are fetched"
        })
    } else {
        return res.json({
            success: true,
            result: findStatus,
            message: "complaints are fetched"
        })
    }
}






