const userSchema = require("../../models/application/user");
const twilio = require("../../views/otp")
let Twilio = new twilio();
const emailValidator = require("email-validator");
const visitPgSChema = require("../../models/application/visitPgModel")
const pgSchema = require("../../models/admin/pgSchema")
const jwt = require("jsonwebtoken");

//sendOTP
exports.sendOTP = async (req, res, next) => {

    let phone = req.body.phone;
    //validatePhone
    if (phone.length != 10) {
        return res.json({
            success: false,
            message: "Please enter valid phone number"
        });
    }

    //OTP
    let OTP = "1212";
    //checkRegister
    const userInfo = await userSchema.findOne({ phone: phone }).exec();
    if (userInfo) {
        const payload = {
            user: {
                id: userInfo._id,
            },
        };
        const token = jwt.sign(payload, process.env.JWT_KEY, {
            expiresIn: "90d",
        });
        if (token) {
            return res.json({
                success: true,
                result: userInfo,
                token: token,
                OTP: OTP,
                message: "Already registered.."
            });
        }
        return res.json({
            success: true,
            result: userInfo,
            token: token,
            OTP: OTP,
            message: "Already registered.."
        });
    }
    else {
        return res.json({
            success: true,
            OTP: OTP,
            message: "OTP sent successfully.."
        });
    }
}

//register
exports.register = async (req, res, next) => {

    let { userName, email, gender, phone, isAcceptTermsAndCondition } = req.body;
    let countryCode = "+91";
    let validate = emailValidator.validate(email);
    if (!validate) {
        return res.json({
            success: false,
            message: "Please enter valid email"
        });
    }

    const data = new userSchema({
        userName: userName,
        email: email,
        gender: gender,
        countryCode: countryCode,
        isPhone_verified: true,
        phone: phone,
        isAcceptTermsAndCondition: isAcceptTermsAndCondition
    });
    const saveData = await data.save();
    if (saveData) {

        const payload = {
            user: {
                id: saveData._id,
            },
        };
        const token = jwt.sign(payload, process.env.JWT_KEY, {
            expiresIn: "90d",
        });
        if (token) {
            return res.json({
                success: true,
                result: saveData,
                token: token,
                message: "Successfully registered!"
            });
        }
    }
    else {
        return res.json({
            success: false,
            message: "Error occured!"
        });
    }
}

//updateUserInfo
module.exports.updateUserInfo = async (req, res, next) => {

    let user_id = req.user_id;
    console.log(user_id)
    let { userName, email, gender } = req.body;
    const updateDetail = await userSchema.findOneAndUpdate(
        { _id: user_id },
        {
            $set: {
                userName: userName,
                email: email,
                gender: gender
            }
        },
        { new: true }
    );
    if (updateDetail) {
        return res.json({
            success: true,
            message: "Successfully Updated.."
        })
    }
    else {
        return res.json({
            success: false,
            message: "Error occured.."
        })
    }

}



module.exports.planToVisit = async (req, res, next) => {
    let userId = req.user_id;
    const { pgId, visitingDate, visitingTime } = req.body;
    const savePgvisit = new visitPgSChema({
        user_id: userId,
        pg_id: pgId,
        dateToVisit: visitingDate,
        TimeToVisit: visitingTime
    })

    const saveInDb = await savePgvisit.save();
    if (saveInDb) {
        return res.json({
            success: true,
            message: "Added Successfully"
        })
    } else {
        return res.json({
            success: false,
            message: "Date is not added yet"
        })
    }
}

// @desc get visited PG
// @route get api/v1/getPgVisit
// access public 
module.exports.getPgVisit = async (req, res, next) => {

    let getVisitSchema = await visitPgSChema.distinct("pg_id", {
        user_id: req.user_id,
    });
    console.log(getVisitSchema)
    if (getVisitSchema.length) {
        let arr = [];
        getVisitSchema.forEach(async (data, i) => {
            arr.push(data.pgId)
            if (i + 1 == getVisitSchema.length) {
                const allPgList = await pgSchema.find({
                    _id: arr
                });
                if (allPgList.length) {
                    return res.json({
                        success: true,
                        result: allPgList,
                        message: "Fetched Successfully"
                    })
                }
                else {
                    return res.json({
                        success: false,
                        message: "No Data Found"
                    })
                }
            }
        })
    }
    else {
        return res.json({
            success: false,
            message: "No Data Found"
        })
    }
}