const advancePaymentSchema = require("../../models/booking/advance")
const transactionSchema = require("../../models/booking/transaction")
const activeServiceSchema = require("../../models/admin/activeServices")
const { getServices } = require("../admin/owner")
const notificationSchema = require("../../models/application/notification")



// @desc new advances
// @route post api/v1/newAdvancePayment
// access public
module.exports.newAdvancePayment = async (req, res, next) => {
    let advanceData = new advancePaymentSchema({
        user_id: req.user_id,
        pg_id: req.body.pg_id,
        room_id: req.body.room_id,
        bed_id: req.body.bed_id,
        activeServiceId: req.body.activeServiceId,
        title: req.body.title,
        razorPayId: req.body.razorPayId,
        Amount: req.body.Amount,
        DepositAmount:req.body.DepositAmount,
        MonthlyRent:req.body.MonthlyRent,
        ServiceAmount:req.body.ServiceAmount,
        TotalAmount:req.body.TotalAmount,
        BalanceAmount:req.body.BalanceAmount,
        status: req.body.status,
    })
    await advanceData.save();
    
    if (advanceData) {
        transaction(res, advanceData, req.user_id, req.body.pg_id, req.body.title, req.body.razorPayId, req.body.Amount, req.body.status)
        const updateNotification = new notificationSchema({
            user_id: req.user_id,
            type:3,
            seen: false,
            title:"Amount Paid",
            message:"You have paid amount " + Amount ,
            amount:Amount,
            created_at:Date.now()
            });
            const saveNotificationData = await updateNotification.save();
            if(saveNotificationData){
                sendNotification(req.user_id, 3);
                return res.json({
                    success:false,
                data:advanceData,
            message:"Payment added "})
            }
        }
    else {
        return res.json({
            success: false,
            message: "Error in save data"
        })
    }
}

// @desc get advances
// @route get api/v1/getAdvancePayment
// access public
module.exports.getAdvancePayment = async (req, res, next) => {
    let getAdvance = await advancePaymentSchema.find()
        .populate("pg_id", "pgOwnerName pgName addressLine1 city state")
        .populate("user_id", "userName gender phone email")
        .populate("room_id", "sharing roomNumber floorNo roomDeposit basicRentPerMonth")

        let array = [];
        let getDistinct = await advancePaymentSchema.distinct("activeServiceId")

        getDistinct.forEach(async (data)=>{

        

        if(data.length){
            const getServices = await activeServiceSchema.find({_id:data})
            array.push(getServices)
        }
        console.log(array)
    })
                if (getAdvance.length) {
                    return res.json({
                        success: true,
                        data: getAdvance,
                        service:array,
                        message: "fetched Successfully",
                    });
                }
            

    else {
        return res.json({
            success: false,
            message: "No Data Found"
        })
    }
}

// @desc get user transaction
// @route get api/v1/getTransactionUser
// access public
module.exports.getTransactionUser = async (req, res, next) => {
    let getAdvance = await transactionSchema.find({ user_id: req.user_id }).populate("pg_id", "pgName pgNumber addressLine1 city state pgImage");
    if (getAdvance.length) {
        return res.json({
            success: true,
            data: getAdvance,
            message: "fetched Successfully",
        });
    }
    else {
        return res.json({
            success: false,
            message: "No Data Found"
        })
    }
}


async function transaction(res, advanceData, user_id, pg_id, title, razorPayId, Amount, status) {
    let transactionData = new transactionSchema({
        user_id: user_id,
        pg_id: pg_id,
        title: title,
        razorPayId: razorPayId,
        amount: Amount,
        status: status,
    })
    await transactionData.save();
    if (transactionData) {
        return res.json({
            success: true,
            data: advanceData,
            message: "Data registered Successfully",
        });
    } else {
        return res.json({
            success: false,
            message: "Error in save data"
        })
    }

}
