const city = require("../models/admin/city");
const state = require("../models/admin/state")


module.exports.updateStateId = async function(){

    let stateNameList = await state.distinct("stateName",{});
    stateNameList.forEach(async(data)=>{
        
        const stateId = await state.findOne({stateName: data});
        const updated = await city.updateMany(
            {stateName: data},
            {
                $set:{state_id: stateId._id}
            },
            {new: true}
        );

    })

}