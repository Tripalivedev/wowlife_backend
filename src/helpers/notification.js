global.admin = require("firebase-admin");
const serviceAccount = require("../serviceAccountkey.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://wowlife-89469-default-rtdb.firebaseio.com"
  });

  const Users = require("../models/application/user")
  const {addNotification} = require("../controllers/application/notification")

module.exports.verifyGCMToken = function (fcmToken) {
    console.log(1);
    return admin.messaging().send({
        token: fcmToken
    }, true)
}

module.exports.sendNotification = async function (sender,type) {
    var message;
    var title;

    if (type == 0) {
        title = 'New Complaint';
        message = ' You have raised a complaint';

    }
    else if (type == 1) {
        title = 'Complaint Status';
        message = 'Your complaint has been completed';
    }

    else if(type== 2){
        title = " Comment for your Request",
        message = "Admin added a comment for your Request"
    }
    else if(type == 3){
        title="Amount Paid",
        message="you have paid amount for a Pg"
    }
    //     message1 = ' others commented on  your Post'
    // }
    // else if (type == 2) {
    //     title = 'New follow';
    //     message = '  started following you';
    //     message1 = ' Others started following you'
    // }
    // else if (type == 3) {
    //     title = 'Follow Request';
    //     message = ' Request to follow you';
    //     message1 = ' Others  Request to follow you'
    // }
    // else if (type == 4) {
    //     title = 'Accepted';
    //     message = '  Accepted your Request';
    //     message1 = ' Others Accepted your Request '
    // }
    // else if (type == 5) {
    //     title = 'Received New Message';
    //     message = '  New Message';
    //     message1 = ' New Message '
    // }
    // else if (type == 6) {
    //     title = 'Relooped your post';
    //     message = '  started Relooped you';
    //     message1 = ' Others Relooped you'
    // }
    //getUserInfo
    // 
    const senderDetails = await Users.find({ _id: sender }, { _id: 0, username: 1,gcm_token:1})
    console.log(senderDetails)
    if (senderDetails){
        var registrationTokens = [
            senderDetails.gcm_token
        ];
    }

        var payload = {
            notification: {
                title: title,
                body:senderDetails.username + message,
            }
        };

        admin.messaging().sendToDevice(registrationTokens, payload)
            .then((response) => {
                console.log(registrationTokens, payload);

            })
            .catch((error) => {
                console.log('Notification failed! ' + error)
            });
        
        //updateNotification
    addNotification(sender,type,message,title);

}


