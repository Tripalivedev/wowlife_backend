const mongoose = require("mongoose");

const activeServicePgData = new mongoose.Schema({
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    services: [{
        name: {
            type: String
        },
        value: {
            type: String
        },
        image: {
            type: String
        },
        amount: {
            type: Number,

        }
    }]
})

module.exports = mongoose.model("activeServicePgData", activeServicePgData);