const mongoose = require("mongoose");

const activeService = new mongoose.Schema({
    services: [{
        name: {
            type: String
        },
        imageUrl: {
            type: String
        }
    }]
})

module.exports = mongoose.model("activeService", activeService);