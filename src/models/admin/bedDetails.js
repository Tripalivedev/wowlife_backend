const mongoose = require("mongoose");


const bed = new mongoose.Schema({
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    room_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'roomDetails'
    },
    bedNumber: {
        type: Number,
        default: 0
    },
    booked: {
        type: Boolean,
        default: false
    },
    mobileNumberUser: {
        type: String,
        default: ""
    },
    createdAt: {
        type: Date,
        default: Date.now(),
    },
})

module.exports = mongoose.model("beds", bed)