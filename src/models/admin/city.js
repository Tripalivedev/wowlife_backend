const mongoose = require("mongoose");

const city = new mongoose.Schema({

    _id: {
        type: String
    },
    cityName: {
        type: String,
        default: ""
    },
    stateName: {
        type: String,
        default: ""
    },
    customerCare: {
        type: String,
        default: ""
    },
    isActive: {
        type: Boolean,
        default: false
    },
    phone: {
        type: String,
        default: ""
    },
    uploadPhoto: {
        type: String,
        default: ""
    }
})

module.exports = mongoose.model("cities", city)
