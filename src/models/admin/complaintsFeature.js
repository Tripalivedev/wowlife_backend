const mongoose = require("mongoose");

const complaintsFeatures = new mongoose.Schema({
    comp_id: {
        type: String
    },
    compName: {
        type: String
    },
    name: {
        type: String
    },
    image: [{
        url: {
            type: String
        }
    }],
    items: [],
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("complaintsFeatures", complaintsFeatures);