const mongoose = require("mongoose");

const activefacilitiesPg = new mongoose.Schema({
    facilities: [{
        name: {
            type: String
        },
        imageUrl: {
            type: String
        }
    }]
})

module.exports = mongoose.model("pgfacilities", activefacilitiesPg);