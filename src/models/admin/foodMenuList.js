const mongoose = require("mongoose");

const menuList = new mongoose.Schema({
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    breakfast: [
        {
            day: {
                type: String
            },
            item: {
                type: String
            },
        }
    ],
    lunch: [
        {
            day: {
                type: String
            },
            item: {
                type: String
            },
        }
    ],
    dinner: [
        {
            day: {
                type: String
            },
            item: {
                type: String
            },
        }
    ]
})

module.exports = mongoose.model("menuList", menuList)