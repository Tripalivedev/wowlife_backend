const mongoose = require("mongoose");

const offerMaster = new mongoose.Schema({
    name: {
        type: String
    },
    percentage: {
        type: String
    },
    code: {
        type: String
    },
    expireDate: {
        type: String
    },
    image: {
        type: String
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    
})

module.exports = mongoose.model("offerMaster", offerMaster);