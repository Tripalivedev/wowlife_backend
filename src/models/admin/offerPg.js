const mongoose = require("mongoose");

const offerPg = new mongoose.Schema({
    pg_id: {
        type: String
    },
    offer_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'offerMaster'
    },
    isActive:{
        type:Boolean,
        default:false
    }
});

module.exports = mongoose.model("offerPg", offerPg);