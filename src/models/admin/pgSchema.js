const mongoose = require("mongoose");

const pgDetails = new mongoose.Schema({
    pgName: {
        type: String,
        default: ""
    },
    pgOwnerName: {
        type: String,
        default: ""
    },
    pgNumber: {
        type: String,
        default: ""
    },
    pgOwnerEmail: {
        type: String,
        default: ""
    },
    pgOwnerNumber: {
        type: String,
        default: ""
    },
    forGender: {
        type: String,
        default: ""
    },
    state: {
        type: String,
        default: ""
    },
    city: {
        type: String,
        default: ""
    },
    landmark: {
        type: String,
        default: ""
    },
    addressLine1: {
        type: String,
        default: ""
    },
    addressLine2: {
        type: String,
        default: ""
    },
    thumbnail: {
        type: String,
        default: ""
    },
    pgImage: [{
        url: {
            type: String
        }
    }],
    noOfFloor: {
        type: String,
        default: ""
    },
    totalNumberOfRoom: {
        type: Number,
        default: 0
    },
    totalNumberOfBeds: {
        type: Number,
        default: 0
    },
    facilities: [{
        name: {
            type: String
        },
        iconUrl: {
            type: String
        }
    }],
    noOfRoomsPerFloor: [{
        floorName: {
            type: String
        },
        totalRoom: {
            type: String
        },
    }],
    isActive: {
        type: Boolean,
        default: false
    },
    rating: {
        type: String,
        default: ""
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    isFavourite: {
        type: Boolean,
        default: false
    }
});

module.exports = mongoose.model("pgDetails", pgDetails);