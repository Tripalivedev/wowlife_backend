const mongoose = require("mongoose");

const room = new mongoose.Schema({
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    bedId: {
        type: mongoose.Schema.Types.ObjectId,
    },
    roomNumber: {
        type: Number,
        default: 0
    },
    sharing: {
        type: Number,
        default: 0
    },
    floorNo: {
        type: Number,
        default: 0
    },
    basicRentPerMonth: {
        type: Number,
        default: 0
    },
    roomDeposit: {
        type: Number,
        default: 0
    },
    roomType: {
        type: String,
        default: ""
    },
    facilities: [{
        name: {
            type: String
        },
        iconUrl: {
            type: String
        }
    }],
    roomImage: [{
        url: {
            type: String
        }
    }],
    createdAt: {
        type: Date,
        default: Date.now(),
    },
})

module.exports = mongoose.model("roomDetails", room)