const mongoose = require("mongoose");

const roomFacilities = new mongoose.Schema({
    facilities: [{
        name: {
            type: String
        },
        imageUrl: {
            type: String
        }
    }]
})

module.exports = mongoose.model("roomFacilities", roomFacilities);