const mongoose = require("mongoose");

const state = new mongoose.Schema({

    _id: {
        type: String
    },
    stateName: {
        type: String,
        default: ""
    },
    customerCare: {
        type: String,
        default: ""
    },
    phone: {
        type: String,
        default: ""
    },
    isActive: {
        type: Boolean,
        default: false
    },
    uploadPhoto: {
        type: String,
        default: ""
    }
})

module.exports = mongoose.model("states", state)
