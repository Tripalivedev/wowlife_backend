const mongoose = require("mongoose");

const useradmin = new mongoose.Schema({
    userName: {
        type: String,
        default: ""
    },
    password: {
        type: String,
        default: ""
    }
})

module.exports = mongoose.model("useradmins",useradmin)
