const mongoose = require("mongoose");

const complaintsUser = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    complaint_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'complaintsFeatures'
    },
    title: {
        type: String,
        default: "",
    },
    complaint_feature: {
        type: String,
        default: "",
    },
    things: {
        type: String,
        default: "",
    },
    description: {
        type: String,
        default: "",
    },
    image: {
        type: String,
        default: "",
    },
    status: {
        type: String,
        default: "",
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model("complaintsUser", complaintsUser);