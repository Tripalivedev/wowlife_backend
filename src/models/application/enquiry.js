const mongoose = require("mongoose");

const enquiry = new mongoose.Schema({
    ownerName: {
        type: String,
        default: "",
    },
    phoneNumber: {
        type: String,
        default: "",
    },
    email: {
        type: String,
        default: "",
    },
    hostelName: {
        type: String,
        default: "",
    },
    numberofBeds: {
        type: String,
        default: "",
    },
    buildingRent: {
        type: String,
        default: "",
    },
    buildingSquareFeet: {
        type: String,
        default: "",
    },
    facility: {
        type: String,
        default: "",
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model("enquiries", enquiry);


