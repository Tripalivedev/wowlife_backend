const mongoose = require("mongoose");

const favourite = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    pg_id: [{
        pgId: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "pgDetails"
        }
    }]
});

module.exports = mongoose.model("favourite", favourite);