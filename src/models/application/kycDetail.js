const mongoose = require("mongoose");

const kycDetail = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    aadharCardImage: {
        type: String,
        default: ""
    },
    aadharCardNo: {
        type: String,
        default: ""
    },
    idproofImage: {
        type: String,
        default: ""
    },
    idproofNo: {
        type: String,
        default: ""
    },
    DOB: {
        type: String,
        default: ""
    },
    permanentAddress1: {
        type: String,
        default: ""
    },
    permanentAddress2: {
        type: String,
        default: ""
    },
    occupationDetail: {
        type: String,
        default: ""
    },
    localGuardianPhoneNo: {
        type: String,
        default: ""
    },
    localGuardianAddress: {
        type: String,
        default: ""
    },
    acknowledgeAndEmergency: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
});

module.exports = mongoose.model("kycDetails", kycDetail);
