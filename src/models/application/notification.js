const mongoose = require("mongoose");


const Notification = new mongoose.Schema({
user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "users"
  },
  title: {
    type: String
  },
  message: {
    type: String
  },
  seen:{
    type:Boolean,
    default:false
  },
  type:{
    type:Number 
  },
  amount:{
    type:Number
  },
  created_at:{
    type:Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("Notification", Notification);
