const mongoose = require("mongoose");

const savedPlaces = new mongoose.Schema({
    user_id: {
        type: String
    },
    places: [{
        title: {
            type: String
        },
        lat: {
            type: String
        },
        lang:{
            type:String
        }
    }]
});

module.exports = mongoose.model("savedPlaces", savedPlaces);