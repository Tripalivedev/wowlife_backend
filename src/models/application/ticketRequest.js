const mongoose = require("mongoose");
const { stringify } = require("querystring");

const ticketRequest = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    },
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    amenities: {
        type: String,
        default: "",
    },
    description: {
        type: String,
        default: "",
    },
    comment:{
        type:String,
        default:"",
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
});

module.exports = mongoose.model("ticketRequests", ticketRequest);