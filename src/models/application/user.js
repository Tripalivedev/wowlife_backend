const mongoose = require("mongoose");

const user = new mongoose.Schema({
    userName: {
        type: String,
        default: ""
    },
    gender: {
        type: String,
        default: ""
    },
    countryCode: {
        type: String,
        default: ""
    },
    phone: {
        type: String,
        default: ""
    },
    isPhone_verified: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        default: ""
    },
    isUpdateWhatsapp: {
        type: Boolean,
        default: false
    },
    bookedBedId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "beds"
    },
    isAcceptTermsAndCondition: {
        type: Boolean,
        default: false
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("users", user);
