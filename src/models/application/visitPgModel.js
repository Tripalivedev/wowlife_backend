const mongoose = require("mongoose");

const visitPg = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    dateToVisit: {
        type: String,
        default: ""
    },
    TimeToVisit: {
        type: String,
        default: ""
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
});

module.exports = mongoose.model("visitPg", visitPg);
