const mongoose = require("mongoose");

const transaction = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    pg_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'pgDetails'
    },
    room_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'roomDetails'
    },
    bed_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "beds"
    },
    service_id:{
        type: mongoose.Schema.Types.ObjectId,
        ref: "activeService"
    },
    activeServiceId: [],
    title: {
        type: String,
        default: ""
    },
    razorPayId: {
        type: String,
        default: ""
    },
    Amount: {
        type: Number,
        default: 0
    },
    DepositAmount:{
        type: Number,
        default: 0
    },
    MonthlyRent:{
        type: Number,
        default: 0
    },
    ServiceAmount:{
        type: Number,
        default: 0
    },
    TotalAmount:{
        type: Number,
        default: 0
    },
    BalanceAmount:{
        type: Number,
        default: 0
    },
    status: {
        type: String,
        default: ""
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("transaction", transaction);