const mongoose = require("mongoose");

const userbooking = new mongoose.Schema({
    user_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    bed_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "beds"
    },
    roomNumber: {
        type: Number,
        default: 0
    },
    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = mongoose.model("userbooking", userbooking);