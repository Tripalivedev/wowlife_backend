const express = require("express");
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const { addCity, createCityList, getCityList,getPgByCity } = require("../../controllers/admin/city");
const router = express.Router();
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

router.post("/createCityList",
catch_error(createCityList));

router.get("/getCityList",
checkQuery("stateName"),
validateRequest,
catch_error(getCityList));

router.post("/addCity",checkRequestBodyParams("customerCare"),
checkRequestBodyParams("id"),
checkRequestBodyParams("phone"),
checkRequestBodyParams("uploadPhoto"),
validateRequest, catch_error(addCity));

router.get("/getPgByCity",
checkQuery("cityName"),
validateRequest,
catch_error(getPgByCity))

module.exports = router
