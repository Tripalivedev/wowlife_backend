const express = require("express");
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const { createFacilities, getFacilities, createActiveService, createComplaints, createComplaintsFeature, getActiveService, createRoomFacilities, getRoomFacilities, getComplaints, getComplaintsById, createOffer, getAllOffer } = require("../../controllers/admin/facilitiesAndServices");
const router = express.Router();
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

//Create new facilities
router.post("/createFacilities",
    catch_error(createFacilities));

//get all facilities
router.get("/getFacilities", catch_error(getFacilities));

//Create new Active Service
router.post("/createActiveService",
    catch_error(createActiveService));

//get All Active Service
router.get("/getActiveService", catch_error(getActiveService));

//Create new room facilities
router.post("/createRoomFacilities", catch_error(createRoomFacilities));

//get all room facilities
router.get("/getRoomFacilities", catch_error(getRoomFacilities));

//Create new complaint
router.post("/createComplaints", catch_error(createComplaints));

//Create New complaints Features
router.post("/createComplaintsFeature", catch_error(createComplaintsFeature));

// get main complaints
router.get("/getComplaints", catch_error(getComplaints))

//get complaints features
router.get("/getComplaintsById", catch_error(getComplaintsById))

//Create New offer Master
router.post("/createOffer", catch_error(createOffer));

//get all offer data
router.get("/getAllOffer", catch_error(getAllOffer))


module.exports = router;
