const express = require("express");
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");

const { addPgDetails, addServices, addMenuList, addRoomDetails, submitRoom, getAllPg, getAddState, getAddCity, EditPG, editServices, editMenuList, editRoomDetails, getPgDetails, getServices, getMenuLists, getRoomData, getAllDataPg, addOfferPg, checkNumber } = require("../../controllers/admin/owner");

// const { addPgDetails, addServices, addMenuList, addRoomDetails, submitRoom, getAllPg, getCity, getState } = require("../../controllers/admin/owner");

const router = express.Router();
// try catch
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

// add pg
router.post("/addPgDetails",
    checkRequestBodyParams("pgOwnerName"),
    checkRequestBodyParams("pgOwnerEmail"),
    checkRequestBodyParams("pgOwnerNumber"),
    checkRequestBodyParams("pgName"),
    checkRequestBodyParams("gender"),
    checkRequestBodyParams("state"),
    checkRequestBodyParams("city"),
    checkRequestBodyParams("landmark"),
    checkRequestBodyParams("addressLine1"),
    checkRequestBodyParams("addressLine2"),
    checkRequestBodyParams("thumbnail"),
    validateRequest,
    catch_error(addPgDetails));



//Add Services
router.post("/addServices",
    checkRequestBodyParams("pgId"),
    validateRequest,
    catch_error(addServices));


//Add MenuList
router.post("/addMenuList",
    checkRequestBodyParams("pgId"),
    validateRequest,
    catch_error(addMenuList));


//Add Room And Bed
router.post("/addRoomDetails",
    checkRequestBodyParams("roomNumber"),
    checkRequestBodyParams("numberOfBeds"),
    checkRequestBodyParams("basicRentPerMonth"),
    checkRequestBodyParams("roomDeposit"),
    checkRequestBodyParams("floorNo"),
    checkRequestBodyParams("roomType"),
    validateRequest,
    catch_error(addRoomDetails));



//Add Submit Room Form
router.post("/submitRoom",
    checkRequestBodyParams("pgId"),
    validateRequest,
    catch_error(submitRoom));


//get All the PG List
router.get("/getAllPg", catch_error(getAllPg));

//get All State
router.get("/getAddState", catch_error(getAddState));


//get all data
router.get("/getAllDataPg",
    checkQuery("pgId"),
    validateRequest,
    catch_error(getAllDataPg));

//get All City
router.get("/getAddCity",
    checkQuery("stateName"),
    validateRequest,
    catch_error(getAddCity));

//Edit PG
router.post("/EditPG",
    checkRequestBodyParams("pgId"),
    validateRequest,
    catch_error(EditPG))

//edit services

router.post("/editServices",
    checkRequestBodyParams("serviceId"),
    validateRequest,
    catch_error(editServices))


//edit MenuList

router.post("/editMenuList",
    checkRequestBodyParams("menuId"),
    validateRequest,
    catch_error(editMenuList))

//edit RoomDetails

router.post("/editRoomDetails",
    checkRequestBodyParams("roomId"),
    validateRequest,
    catch_error(editRoomDetails))




//get pg datas

router.get("/getPgDetails",
    checkQuery("pgId"),
    validateRequest,
    catch_error(getPgDetails))


//get services

router.get("/getServices",
    checkQuery("pgId"),
    validateRequest,
    catch_error(getServices))



//get menulist

router.get("/getMenuLists",
    checkQuery("pgId"),
    validateRequest,
    catch_error(getMenuLists))


//get room details
router.get("/getRoomData",
    checkQuery("roomId"),
    validateRequest,
    catch_error(getRoomData));



//get Pg offer details
router.post("/addOfferPg",
    validateRequest,
    catch_error(addOfferPg))

//check number Available
router.get("/checkNumber", catch_error(checkNumber));


module.exports = router
