const express = require("express");
const { addState, createStateList, getStateList,getPgByState } = require("../../controllers/admin/state");
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");

const router = express.Router();
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

router.post("/createStateList",
catch_error(createStateList));

router.get("/getStateList", catch_error(getStateList));

router.post("/addState",
checkRequestBodyParams("stateName"),
checkRequestBodyParams("customerCare"),
checkRequestBodyParams("phone"),
checkRequestBodyParams("uploadPhoto"),
validateRequest,
catch_error(addState));


router.get("/getPgByState",
checkQuery("stateName"),
validateRequest,
catch_error(getPgByState))

module.exports = router
