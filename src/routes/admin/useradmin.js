const express = require("express");
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const { register, login, updateUserDetails, fetchUser, dashboard, fetchUserById,getComplaintsforAdmin,updateStatusOfComplaint,getUserRequests,addComment,updateStatusOfRequests } = require("../../controllers/admin/useradmin");
const router = express.Router();
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

// router.post("/adminRegister",register)

router.post("/adminLogin",
    catch_error(login));

router.post("/updateUserDetails",
checkRequestBodyParams("user_id"),
validateRequest,
    catch_error(updateUserDetails));

router.get("/fetchUser",
    catch_error(fetchUser));

router.get("/fetchUserById",
checkQuery("userId"),
validateRequest,
    catch_error(fetchUserById));

// Dashboard
router.get("/dashboard", catch_error(dashboard));

//getcomplaintsForAll

router.get("/getComplaintsforAdmin",catch_error(getComplaintsforAdmin))


//UPDATESTATUS
router.post("/updateStatusOfComplaint",
catch_error(updateStatusOfComplaint))


router.get("/getUserRequests",catch_error(getUserRequests))

router.post("/addComment",catch_error(addComment))


router.post("/updateStatusOfRequests",
catch_error(updateStatusOfRequests))

module.exports = router
