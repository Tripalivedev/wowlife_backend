const express = require("express");
const router = express.Router();
// Validators
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
//controller
const { submit_enquiry, get_enquiry, getAllComplaints, getSingleComplaints } = require("../../controllers/application/enquiry");
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

// create enquiry
router.post("/submit_enquiry",
    checkRequestBodyParams("ownerName"),
    checkRequestBodyParams("phone"),
    checkRequestBodyParams("email"),
    checkRequestBodyParams("hostel"),
    checkRequestBodyParams("numberofBeds"),
    checkRequestBodyParams("buildingRent"),
    checkRequestBodyParams("buildingSquareFeet"),
    checkRequestBodyParams("facility"),
    validateRequest,
    catch_error(submit_enquiry));

//get enquiry
router.get("/get_enquiry", catch_error(get_enquiry));

// // Get All complaints
router.get("/getAllComplaints", catch_error(getAllComplaints));

// // Get Single Complaint List
router.get("/getSingleComplaints", catch_error(getSingleComplaints));

module.exports = router;