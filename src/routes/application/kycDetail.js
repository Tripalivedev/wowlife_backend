const express = require("express");
const { createKycDetails } = require("../../controllers/application/kycDetail");
const router = express.Router();
// find error
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const { checkSession } = require("../../middlewares/checkAuth");
const { checkIsactive } = require("../../middlewares/checkActive");

router.post("/createKycDetails",
    checkSession,
    checkIsactive,
    checkRequestBodyParams("aadharCardImage"),
    checkRequestBodyParams("aadharCardNo"),
    checkRequestBodyParams("idproofImage"),
    checkRequestBodyParams("idproofNo"),
    checkRequestBodyParams("DOB"),
    checkRequestBodyParams("permanentAddress1"),
    checkRequestBodyParams("permanentAddress2"),
    checkRequestBodyParams("occupationDetail"),
    checkRequestBodyParams("localGuardianPhoneNo"),
    checkRequestBodyParams("localGuardianAddress"),
    checkRequestBodyParams("acknowledgeAndEmergency"),
    validateRequest,
    catch_error(createKycDetails));

module.exports = router