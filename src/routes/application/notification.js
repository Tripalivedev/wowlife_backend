const express = require("express");
const { getAllNotification, getUnreadCount, updateReadCount } = require("../../controllers/application/notification");
const router = express.Router();
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);
const { checkSession } = require("../../middlewares/checkAuth");
const { checkIsactive } = require("../../middlewares/checkActive");

//getAllNotification
router.get("/getAllNotification",
        // checkSession,
        // checkIsactive,
        catch_error(getAllNotification));

//getUnreadCount
router.get("/getUnreadCount",
        checkSession,
        checkIsactive,
        catch_error(getUnreadCount));

//updateReadCount
router.post("/updateReadCount",
        checkSession,
        checkIsactive,
        catch_error(updateReadCount));


module.exports = router;
