const express = require("express");
const { popularPlaces, popularPlacesPg, getLandMark, filterMain, searchMain, removeFavourite, getSavedPlace, addSavedPlace, ratePg, getOnePg, getState, nearByPg, getCity, getFavourite, addFavourite, pgRoomFilter, pgMenu, getAllDataRoom } = require("../../controllers/application/pgController");
const router = express.Router();


const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const { checkSession } = require("../../middlewares/checkAuth");
const { checkIsactive } = require("../../middlewares/checkActive");
//find error
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

// get all popular places
router.get("/popularPlaces",
    checkSession,
    checkIsactive,
    checkQuery("state"),
    checkQuery("city"),
    validateRequest,
    catch_error(popularPlaces));

// popular place pg list
router.get("/popularPlacesPg",
    checkSession,
    checkIsactive,
    checkQuery("state"),
    checkQuery("city"),
    checkQuery("landmark"),
    validateRequest,
    catch_error(popularPlacesPg));

// find landmark list using city
router.get("/getLandMark", checkSession,
    checkIsactive,
    checkQuery("state"),
    checkQuery("city"),
    validateRequest,
    catch_error(getLandMark));


//search using Address and pg name
router.get("/searchMain", checkSession,
    checkIsactive,
    checkQuery("state"),
    checkQuery("city"),
    checkQuery("pgName"),
    validateRequest,
    catch_error(searchMain));

//main filter
router.post("/filterMain",
    checkSession,
    checkIsactive,
    catch_error(filterMain));

// Rate PG by User
router.post("/ratePg",
    checkSession,
    checkIsactive,
    checkRequestBodyParams("pgId"),
    checkRequestBodyParams("rating"),
    validateRequest,
    catch_error(ratePg));

// Get a single Pg
router.get("/getOnePg",
    // checkSession,
    // checkIsactive,
    checkQuery("pgId"),
    validateRequest,
    catch_error(getOnePg));

//get All State
router.get("/getState", catch_error(getState));
//get All City
router.get("/getCity",
    checkQuery("state"),
    validateRequest,
    catch_error(getCity));

// Add favourite pg
router.post("/addFavourite",
    checkSession,
    checkIsactive,
    checkRequestBodyParams("pgId"),
    validateRequest,
    catch_error(addFavourite));

// get favourite pg
router.get("/getFavourite",
    checkSession,
    checkIsactive,
    catch_error(getFavourite));

// remove favourite pg
router.get("/removeFavourite",
    checkSession,
    checkIsactive,
    checkQuery("pgId"),
    validateRequest,
    catch_error(removeFavourite));

// Add saved places 
router.post("/addSavedPlace",
    checkSession,
    checkIsactive,
    checkRequestBodyParams("title"),
    checkRequestBodyParams("lat"),
    checkRequestBodyParams("lang"),
    validateRequest,
    catch_error(addSavedPlace));

// get saved places
router.get("/getSavedPlace",
    checkSession,
    checkIsactive,
    catch_error(getSavedPlace));

// Add near by pg
router.get("/nearByPg",
    checkSession,
    checkIsactive,
    checkQuery("state"),
    checkQuery("city"),
    validateRequest,
    catch_error(nearByPg));

//post pg's room details
router.post("/pgRoomFilter",
    checkSession,
    checkIsactive,
    checkRequestBodyParams("pgId"),
    validateRequest,
    catch_error(pgRoomFilter))


//post all data of room 
router.get("/getAllDataRoom",
    checkSession,
    checkIsactive,
    checkQuery("pgId"),
    catch_error(getAllDataRoom))

//get pg menu
router.get("/pgMenu",
    checkSession,
    checkIsactive,
    checkQuery("pgId"),
    validateRequest, catch_error(pgMenu))

module.exports = router
