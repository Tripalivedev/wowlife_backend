const express = require("express");
const router = express.Router();
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
const { checkSession } = require("../../middlewares/checkAuth");
const { checkIsactive } = require("../../middlewares/checkActive");
const { createTicketRequest, getAllTickets, getComplaintsApp, getComplaintsByIdApp, createUserComplaints,getComplaintsOnStatus} = require("../../controllers/application/ticketRequest");



//createTicketRequest
router.post("/createTicketRequest",
        checkSession,
        checkIsactive,
        checkRequestBodyParams("pg_id"),
        checkRequestBodyParams("amenities"),
        checkRequestBodyParams("description"),
        validateRequest,
        catch_error(createTicketRequest)
)

// get All Tickets
router.get("/getAllTickets",
        checkSession,
        checkIsactive,
        catch_error(getAllTickets))

// get main complaints
router.get("/getComplaintsApp",
        checkSession,
        checkIsactive,
        catch_error(getComplaintsApp))

//get complaints features
router.get("/getComplaintsByIdApp",
        checkSession,
        checkIsactive,
        catch_error(getComplaintsByIdApp))

//cerate User complaints
router.post("/createUserComplaints",
        // checkSession,
        // checkIsactive,
        checkRequestBodyParams("pg_id"),
        checkRequestBodyParams("complaint_id"),
        checkRequestBodyParams("title"),
        checkRequestBodyParams("complaint_feature"),
        checkRequestBodyParams("things"),
        checkRequestBodyParams("description"),
        checkRequestBodyParams("image"),
        validateRequest,
        catch_error(createUserComplaints)
)



router.get("/getComplaintsOnStatus",
checkSession,
        checkIsactive,
catch_error(getComplaintsOnStatus))

module.exports = router;


