const express = require("express");
const router = express.Router();
// Validators

const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
//controller
const { sendOTP, register, updateUserInfo, planToVisit, getPgVisit } = require("../../controllers/application/user");
const { checkSession } = require("../../middlewares/checkAuth");
const { checkIsactive } = require("../../middlewares/checkActive");
// find error
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

//sendOTP
router.post("/sendOTP",
        checkRequestBodyParams("phone"),
        validateRequest,
        catch_error(sendOTP));

//register
router.post("/register",
        checkRequestBodyParams("userName"),
        checkRequestBodyParams("email"),
        checkRequestBodyParams("gender"),
        checkRequestBodyParams("phone"),
        checkRequestBodyParams("isAcceptTermsAndCondition"),
        validateRequest,
        catch_error(register));


//updateUserInfo
router.post("/updateUserInfo",
        checkSession,
        checkIsactive,
        catch_error(updateUserInfo));


router.post("/planToVisit",
        checkSession,
        checkIsactive,
        checkRequestBodyParams("pgId"),
        validateRequest,
        catch_error(planToVisit))


router.get("/getPgVisit",
        checkSession,
        checkIsactive,
        catch_error(getPgVisit))



module.exports = router;



