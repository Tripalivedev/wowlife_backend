const express = require("express");
const router = express.Router();
// Validators
const { checkRequestBodyParams, validateRequest, checkParam, checkQuery } = require("../../middlewares/validators");
//controller
const { newAdvancePayment, getAdvancePayment, getTransactionUser } = require("../../controllers/payment/paymentAdvance");
const catch_error = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

const { checkSession } = require("../../middlewares/checkAuth");
const { checkIsactive } = require("../../middlewares/checkActive");

// Get Advance from new user
router.post("/newAdvancePayment",
    checkSession,
    checkIsactive,
    checkRequestBodyParams("pg_id"),
    checkRequestBodyParams("room_id"),
    checkRequestBodyParams("bed_id"),
    checkRequestBodyParams("title"),
    checkRequestBodyParams("razorPayId"),
    checkRequestBodyParams("Amount"),
    validateRequest,
    catch_error(newAdvancePayment));

//get all Advances
router.get("/getAdvancePayment", catch_error(getAdvancePayment));

//get all transaction user
router.get("/getTransactionUser",
    checkSession,
    checkIsactive,
    catch_error(getTransactionUser));

module.exports = router;